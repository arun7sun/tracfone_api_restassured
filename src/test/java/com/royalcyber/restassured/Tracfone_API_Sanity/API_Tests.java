package com.royalcyber.restassured.Tracfone_API_Sanity;

/**
 * @author Arunkumar Ravichandran
 *
 */

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.assertion.*;

import org.testng.Assert;
import org.testng.annotations.Test;

public class API_Tests extends Helper {

	Db_Conn db = new Db_Conn();

	@Test(groups = "tracfone")
	public void tracfone_APIs() throws Exception {
		db.envSetup(env);
		db.setUp(dbProductName);
		String ESN = db.getEsn();
		// String pin = db.getPin();
		String SIM = db.getSim();
		String accountID;

		// Oauth cc token generation
		String accessTokenCC = getAccessTokenCC("TFWebMyAcct_CCU");
		System.out.println("CC oauth token ==>" + accessTokenCC);

		Response resTF2 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenCC)
				.when()
				.get("/pep/validateUserDevice/v2/esn/" + ESN + "?brandName="
						+ brandName + "&language=" + language + "&min=" + ESN
						+ "&sourceSystem=" + sourceSystem + "&timeStamp="
						+ String.format("%d", System.currentTimeMillis()));
		System.out
				.println("API no 2------------------/pep/validateUserDevice/v2/esn/   GET-Method API");
		resTF2.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(resTF2.getBody().jsonPath()
				.getString("status.type"), "SUCCESS",
				"API has failed in this step");

		RequestSpecification reqTF3 = RestAssured.given();
		reqTF3.header("Authorization", "Bearer " + accessTokenCC);
		reqTF3.header("Content-Type", "application/json");
		reqTF3.body(service_QualificationJSON(ESN, SIM, "TECHNOLOGY",
				"application").toString());
		Response resTF3 = reqTF3
				.post("/pep/service-qualification-mgmt/v1/service-qualification");
		System.out
				.println("API no 3-----------------/pep/service-qualification-mgmt/v1/service-qualification \"Category - TECHNOLOGY\"   POST-Method API");
		resTF3.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(resTF3.getBody().jsonPath()
				.getString("status.type"), "SUCCESS",
				"API has failed in this step");

		RequestSpecification reqTF4 = RestAssured.given();
		reqTF4.header("Authorization", "Bearer " + accessTokenCC);
		reqTF4.header("Content-Type", "application/json");
		reqTF4.body(service_QualificationJSON(ESN, SIM, "TECHNOLOGY",
				"application").toString());
		Response resTF4 = reqTF4
				.post("/pep/service-qualification-mgmt/v1/service-qualification");
		System.out
				.println("API no 4----------------/pep/service-qualification-mgmt/v1/service-qualification \"Category - TECHNOLOGY\"   POST-Method API");
		resTF4.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(resTF4.getBody().jsonPath()
				.getString("status.type"), "SUCCESS",
				"API has failed in this step");

		RequestSpecification reqTF5 = RestAssured.given();
		reqTF5.header("Authorization", "Bearer " + accessTokenCC);
		reqTF5.header("Content-Type", "application/json");
		reqTF5.body(service_QualificationJSON(ESN, SIM, "ACTIVATION",
				"application").toString());
		Response resTF5 = reqTF5
				.post("/pep/service-qualification-mgmt/v1/service-qualification");
		System.out
				.println("API no 5---------------/pep/service-qualification-mgmt/v1/service-qualification \"Category - ACTIVATION\"   POST-Method API");
		resTF5.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(resTF5.getBody().jsonPath()
				.getString("status.type"), "SUCCESS",
				"API has failed in this step");

		Response resTF6 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenCC)
				.when()
				.get("/pep/servicePlanSelector/v2/esn/" + ESN + "?brandName="
						+ brandName + "&language=" + language + "&min=" + ESN
						+ "&sourceSystem=" + sourceSystem + "&timeStamp="
						+ String.format("%d", System.currentTimeMillis()));
		System.out
				.println("API no 6---------------/pep/servicePlanSelector/v2/esn/   GET-Method API");
		resTF6.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(resTF6.getBody().jsonPath()
				.getString("status.type"), "SUCCESS",
				"API has failed in this step");

		RequestSpecification reqTF7 = RestAssured.given();
		reqTF7.header("Authorization", "Bearer " + accessTokenCC);
		reqTF7.header("Content-Type", "application/json");
		reqTF7.header("X-Requested-With", "XMLHttpRequest");
		reqTF7.body(createAccountJson(ESN, email).toString());
		Response resTF7 = reqTF7.and().post(
				"/pep/createNewAccount/".concat(email));
		System.out
				.println("API no 7---------------/pep/createNewAccount/   POST-Method API");
		resTF7.then().log().status().and().assertThat().statusCode(200);
		JsonPath jsonPath = new JsonPath(resTF7.asString());
		accountID = jsonPath.getString("response.accountId");
		Assert.assertEquals(resTF7.getBody().jsonPath()
				.getString("status.type"), "SUCCESS",
				"API has failed in this step");

		System.out.println("API no 8----------------------------------------");
		// Oauth ro token generation
		String accessTokenRO = getAccessTokenRO(email, "TFWebMyAcct_RO");
		System.out.println("RO oauth token ==>" + accessTokenRO);

		Response resTF9 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenRO)
				.header("X-Requested-With", "XMLHttpRequest")
				.queryParam("brandName", brandName)
				.queryParam("category", "SERVICE_PLAN")
				.queryParam("channel", "B2C")
				.queryParam("criteria", queryParam_ProductOffering(ESN))
				.queryParam("language", language)
				.queryParam("partyID", partyID)
				.queryParam("sourceSystem", sourceSystem)
				.queryParam("timeStamp",
						String.format("%d", System.currentTimeMillis())).when()
				.get("/pep/catalog-mgmt/v1/productoffering");
		System.out
				.println("API no 9---------------/pep/catalog-mgmt/v1/productoffering   GET-Method API");
		resTF9.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(resTF9.getBody().jsonPath()
				.getString("status.type"), "SUCCESS",
				"API has failed in this step");

		RequestSpecification reqTF10 = RestAssured.given();
		reqTF10.header("Authorization", "Bearer " + accessTokenRO);
		reqTF10.header("Content-Type", "application/json");
		reqTF10.header("X-Requested-With", "XMLHttpRequest");
		reqTF10.body(calculateTax_Payload(ESN, Integer.parseInt(accountID))
				.toString());
		Response resTF10 = reqTF10.and().post("/pep/calculatePurchaseTax/");
		System.out
				.println("API no 10-------------/pep/calculatePurchaseTax/   POST-Method API");
		resTF10.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(
				resTF10.getBody().jsonPath().getString("status.type"),
				"SUCCESS", "API has failed in this step");

		Response resTF11 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenRO)
				.header("X-Requested-With", "XMLHttpRequest")
				.when()
				.get("/pep/getPaymentSource/" + accountID + "?account_id="
						+ accountID + "&brandName=" + brandName + "&language="
						+ language + "&sourceSystem=" + sourceSystem
						+ "&timeStamp="
						+ String.format("%d", System.currentTimeMillis()));
		System.out
				.println("API no 11------------/pep/getPaymentSource/   GET-Method API");
		resTF11.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(
				resTF11.getBody().jsonPath().getString("status.type"),
				"FAILURE", "API has failed in this step");

		RequestSpecification reqTF12 = RestAssured.given();
		reqTF12.header("Authorization", "Bearer " + accessTokenRO);
		reqTF12.header("Content-Type", "application/json");
		reqTF12.header("X-Requested-With", "XMLHttpRequest");
		reqTF12.body(productOrder_PayLoadTF(accountID, ESN, SIM).toString());
		Response resTF12 = reqTF12.and()
				.post("/pep/order-mgmt/v1/productorder");
		System.out
				.println("API no 12------------/pep/order-mgmt/v1/productorder   POST-Method API");
		resTF12.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(
				resTF12.getBody().jsonPath().getString("status.type"),
				"SUCCESS", "API has failed in this step");

		RequestSpecification reqTF13 = RestAssured.given();
		reqTF13.header("Authorization", "Bearer " + accessTokenRO);
		reqTF13.header("Content-Type", "application/json");
		reqTF13.header("X-Requested-With", "XMLHttpRequest");
		reqTF13.body(calculateTax_Payload(ESN, Integer.parseInt(accountID))
				.toString());
		Response resTF13 = reqTF13.and().post("/pep/calculatePurchaseTax/");
		System.out
				.println("API no 13------------/pep/calculatePurchaseTax/ POST-Method API");
		resTF13.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(
				resTF13.getBody().jsonPath().getString("status.type"),
				"SUCCESS", "API has failed in this step");

		Response resTF14 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenRO)
				.header("X-Requested-With", "XMLHttpRequest")
				.queryParam("brandName", brandName)
				.queryParam("language", language)
				.queryParam("partClass", "TFLGL57BL")
				.queryParam("script_id", "STR_8158")
				.queryParam("sourceSystem", sourceSystem)
				.queryParam("timeStamp",
						String.format("%d", System.currentTimeMillis())).when()
				.get("/pep/getScript/scripts/STR_8158");
		System.out
				.println("API no 14------------/pep/getScript/scripts/STR_8158   GET-Method API");
		resTF14.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(
				resTF14.getBody().jsonPath().getString("status.type"),
				"SUCCESS", "API has failed in this step");

		Response resTF15 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenRO)
				.header("X-Requested-With", "XMLHttpRequest")
				.queryParam("brandName", brandName)
				.queryParam("category", "SERVICE_PLAN")
				.queryParam("channel", "B2C")
				.queryParam("criteria", queryParam_ProductOffering(ESN))
				.queryParam("language", language)
				.queryParam("partyID", partyID)
				.queryParam("sourceSystem", sourceSystem)
				.queryParam("timeStamp",
						String.format("%d", System.currentTimeMillis())).when()
				.get("/pep/catalog-mgmt/v1/productoffering");
		System.out
				.println("API no 15------------/pep/catalog-mgmt/v1/productoffering   GET-Method API");
		resTF15.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(
				resTF15.getBody().jsonPath().getString("status.type"),
				"SUCCESS", "API has failed in this step");

		Response resTF16 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenCC)
				.when()
				.get("/pep/validateUserDevice/v2/esn/" + ESN + "?brandName="
						+ brandName + "&language=" + language + "&min=" + ESN
						+ "&sourceSystem=" + sourceSystem + "&timeStamp="
						+ String.format("%d", System.currentTimeMillis()));
		System.out
				.println("API no 16------------/pep/validateUserDevice/v2/esn/   GET-Method API");
		resTF16.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(
				resTF16.getBody().jsonPath().getString("status.type"),
				"SUCCESS", "API has failed in this step");
	}

	@Test(groups = "net10")
	public void Net10_APIs() throws Exception {
		db.envSetup(env);
		db.setUp(dbProductName);
		String ESN = db.getEsn();
		// String pin = db.getPin();
		String SIM = db.getSim();
		String accountID;

		// Oauth cc token generation
		String accessTokenCC = getAccessTokenCC("NET10WebMyAcct_CCU");
		System.out.println("CC oauth token ==>" + accessTokenCC);

		Response resNT2 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenCC)
				.when()
				.get("/pep/servicePlanSelector/v2/?brandName=" + brandName
						+ "&language=" + language + "&min=" + ESN
						+ "&sourceSystem=" + sourceSystem + "&timeStamp="
						+ String.format("%d", System.currentTimeMillis()));
		System.out
				.println("API no 2------------------/pep/servicePlanSelector/v2/   GET-Method API");
		resNT2.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(resNT2.getBody().jsonPath()
				.getString("status.type"), "SUCCESS",
				"API has failed in this step");

		Response resNT3 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenCC)
				.when()
				.get("/pep/validateUserDevice/v2/esn/" + ESN + "?brandName="
						+ brandName + "&language=" + language + "&min=" + ESN
						+ "&sourceSystem=" + sourceSystem + "&timeStamp="
						+ String.format("%d", System.currentTimeMillis()));
		System.out
				.println("API no 3------------------/pep/validateUserDevice/v2/esn/   GET-Method API");
		resNT3.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(resNT3.getBody().jsonPath()
				.getString("status.type"), "SUCCESS",
				"API has failed in this step");

		RequestSpecification reqNT4 = RestAssured.given();
		reqNT4.header("Authorization", "Bearer " + accessTokenCC);
		reqNT4.header("Content-Type", "application/json");
		reqNT4.body(service_QualificationJSON_NTSM(ESN, SIM, "TECHNOLOGY",
				"NET10WEB").toString());
		Response resNT4 = reqNT4
				.post("/pep/service-qualification-mgmt/v1/service-qualification");
		System.out
				.println("API no 4-----------------/pep/service-qualification-mgmt/v1/service-qualification \"Category - TECHNOLOGY\"   POST-Method API");
		resNT4.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(resNT4.getBody().jsonPath()
				.getString("status.type"), "SUCCESS",
				"API has failed in this step");

		RequestSpecification reqNT5 = RestAssured.given();
		reqNT5.header("Authorization", "Bearer " + accessTokenCC);
		reqNT5.header("Content-Type", "application/json");
		reqNT5.body(service_QualificationJSON_NTSM(ESN, SIM, "ACTIVATION",
				"NET10WEB").toString());
		Response resNT5 = reqNT5
				.post("/pep/service-qualification-mgmt/v1/service-qualification");
		System.out
				.println("API no 5-----------------/pep/service-qualification-mgmt/v1/service-qualification \"Category - TECHNOLOGY\"   POST-Method API");
		resNT5.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(resNT5.getBody().jsonPath()
				.getString("status.type"), "SUCCESS",
				"API has failed in this step");

		Response resNT6 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenCC)
				.when()
				.get("/pep/servicePlanSelector/v2/esn/" + ESN + "?brandName="
						+ brandName + "&language=" + language + "&min=" + ESN
						+ "&sourceSystem=" + sourceSystem + "&timeStamp="
						+ String.format("%d", System.currentTimeMillis()));
		System.out
				.println("API no 6---------------/pep/servicePlanSelector/v2/esn/   GET-Method API");
		resNT6.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(resNT6.getBody().jsonPath()
				.getString("status.type"), "SUCCESS",
				"API has failed in this step");

		RequestSpecification reqNT7 = RestAssured.given();
		reqNT7.header("Authorization", "Bearer " + accessTokenCC);
		reqNT7.header("Content-Type", "application/json");
		reqNT7.header("X-Requested-With", "XMLHttpRequest");
		reqNT7.body(createAccountJson(ESN, email).toString());
		Response resNT7 = reqNT7.and().post(
				"/pep/createNewAccount/".concat(email));
		System.out
				.println("API no 7---------------/pep/createNewAccount/   POST-Method API");
		resNT7.then().log().status().and().assertThat().statusCode(200);
		JsonPath jsonPath = new JsonPath(resNT7.asString());
		accountID = jsonPath.getString("response.accountId");
		Assert.assertEquals(resNT7.getBody().jsonPath()
				.getString("status.type"), "SUCCESS",
				"API has failed in this step");

		System.out.println("API no 8----------------------------------------");
		// Oauth ro token generation
		String accessTokenRO = getAccessTokenRO(email, "NET10WebMyAcct_RO");
		System.out.println("RO oauth token ==>" + accessTokenRO);

		RequestSpecification reqNT9 = RestAssured.given();
		reqNT9.header("Authorization", "Bearer " + accessTokenRO);
		reqNT9.header("Content-Type", "application/json");
		reqNT9.header("X-Requested-With", "XMLHttpRequest");
		reqNT9.body(estimate_NTPayload(accountID, ESN).toString());
		Response resNT9 = reqNT9.and().post(
				"/pep/order-mgmt/v1/productorder/estimate");
		System.out
				.println("API no 9---------------/pep/order-mgmt/v1/productorder/estimate   POST-Method API");
		resNT9.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(resNT9.getBody().jsonPath()
				.getString("status.type"), "SUCCESS",
				"API has failed in this step");

		Response resNT10 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenRO)
				.when()
				.get("/pep/getAccountProfile/" + accountID + "?account_id="
						+ accountID + "&brandName=" + brandName + "&language="
						+ language + "&sourceSystem=" + sourceSystem
						+ "&timeStamp="
						+ String.format("%d", System.currentTimeMillis()));
		System.out
				.println("API no 10---------------/pep/getAccountProfile/   GET-Method API");
		resNT10.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(
				resNT10.getBody().jsonPath().getString("status.type"),
				"SUCCESS", "API has failed in this step");

		Response resNT11 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenRO)
				.when()
				.get("/pep/getPaymentSource/" + accountID + "?account_id="
						+ accountID + "&brandName=" + brandName + "&language="
						+ language + "&sourceSystem=" + sourceSystem
						+ "&timeStamp="
						+ String.format("%d", System.currentTimeMillis()));
		System.out
				.println("API no 11---------------/pep/getPaymentSource/   GET-Method API");
		resNT11.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(
				resNT11.getBody().jsonPath().getString("status.type"), "ERROR",
				"API has failed in this step");

		RequestSpecification reqNT12 = RestAssured.given();
		reqNT12.header("Authorization", "Bearer " + accessTokenRO);
		reqNT12.header("Content-Type", "application/json");
		reqNT12.header("X-Requested-With", "XMLHttpRequest");
		reqNT12.body(productOrder_PayloadNT(accountID, ESN, SIM).toString());
		Response resNT12 = reqNT12.and()
				.post("/pep/order-mgmt/v2/productorder");
		System.out
				.println("API no 12---------------/pep/order-mgmt/v2/productorder   POST-Method API");
		resNT12.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(
				resNT12.getBody().jsonPath().getString("status.type"),
				"SUCCESS", "API has failed in this step");

		Response resNT13 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenRO)
				.when()
				.get("/pep/getAccountDetails/v2/" + accountID + "?brandName="
						+ brandName + "&language=" + language
						+ "&sourceSystem=" + sourceSystem + "&timeStamp="
						+ String.format("%d", System.currentTimeMillis()));
		System.out
				.println("API no 13---------------/pep/getAccountDetails/v2/   GET-Method API");
		resNT13.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(
				resNT13.getBody().jsonPath().getString("status.type"),
				"SUCCESS", "API has failed in this step");

		Response resNT14 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenRO)
				.when()
				.get("/pep/getScript/scripts/STR_8158?brandName="
						+ brandName
						+ "&language="
						+ language
						+ "&partClass=NTLG329G&script_id=STR_8158&sourceSystem="
						+ sourceSystem + "&timeStamp="
						+ String.format("%d", System.currentTimeMillis()));
		System.out
				.println("API no 14---------------/pep/getScript/scripts/STR_8158   GET-Method API");
		resNT14.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(
				resNT14.getBody().jsonPath().getString("status.type"),
				"SUCCESS", "API has failed in this step");

	}

	@Test(groups = "total_wireless")
	public void Total_wireless_APIs() throws Exception {
		db.envSetup(env);
		db.setUp(dbProductName);
		String ESN = db.getEsn();
		// String pin = db.getPin();
		String SIM = db.getSim();
		String accountID;
		String groupID;

		// Oauth cc token generation
		String accessTokenCC = getAccessTokenCC("TWWebMyAcct_CCU");
		System.out.println("CC oauth token ==>" + accessTokenCC);

		Response resTW2 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenCC)
				.when()
				.get("/pep/servicePlanSelector/v2/?brandName=" + brandName
						+ "&language=" + language + "&sourceSystem="
						+ sourceSystem + "&timeStamp="
						+ String.format("%d", System.currentTimeMillis()));
		System.out
				.println("API no 2------------------/pep/servicePlanSelector/v2/   GET-Method API");
		resTW2.then().log().body();

		Response resTW3 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenCC)
				.when()
				.get("/pep/validateUserDevice/v2/esn/" + ESN + "?brandName="
						+ brandName + "&language=" + language + "&min=" + ESN
						+ "&sourceSystem=" + sourceSystem + "&timeStamp="
						+ String.format("%d", System.currentTimeMillis()));
		System.out
				.println("API no 3------------------/pep/validateUserDevice/v2/esn/   GET-Method API");
		resTW3.then().log().body();

		RequestSpecification reqTW4 = RestAssured.given();
		reqTW4.header("Authorization", "Bearer " + accessTokenCC);
		reqTW4.header("Content-Type", "application/json");
		reqTW4.body(service_QualificationJSONTW(ESN, SIM, "TECHNOLOGY",
				"internal").toString());
		Response resTW4 = reqTW4
				.post("/pep/service-qualification-mgmt/v1/service-qualification");
		System.out
				.println("API no 4-----------------/pep/service-qualification-mgmt/v1/service-qualification \"Category - TECHNOLOGY\"   POST-Method API");
		resTW4.then().log().body();

		RequestSpecification reqTW5 = RestAssured.given();
		reqTW5.header("Authorization", "Bearer " + accessTokenCC);
		reqTW5.header("Content-Type", "application/json");
		reqTW5.body(service_QualificationJSONTW(ESN, SIM, "ACTIVATION",
				"internal").toString());
		Response resTW5 = reqTW5
				.post("/pep/service-qualification-mgmt/v1/service-qualification");
		System.out
				.println("API no 5-----------------/pep/service-qualification-mgmt/v1/service-qualification \"Category - TECHNOLOGY\"   POST-Method API");
		resTW5.then().log().body();

		Response resTW6 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenCC)
				.when()
				.get("/pep/servicePlanSelector/v2/esn/" + ESN + "?brandName="
						+ brandName + "&esn=" + ESN + "&language=" + language
						+ "&sourceSystem=" + sourceSystem + "&timeStamp="
						+ String.format("%d", System.currentTimeMillis()));
		System.out
				.println("API no 6---------------/pep/servicePlanSelector/v2/esn/   GET-Method API");
		resTW6.then().log().body();

		RequestSpecification reqTW7 = RestAssured.given();
		reqTW7.header("Authorization", "Bearer " + accessTokenCC);
		reqTW7.header("Content-Type", "application/json");
		reqTW7.header("X-Requested-With", "XMLHttpRequest");
		reqTW7.body(createAccountJson(ESN, email).toString());
		Response resTW7 = reqTW7.and().post(
				"/pep/createNewAccount/".concat(email));
		System.out
				.println("API no 7---------------/pep/createNewAccount/   POST-Method API");
		resTW7.then().log().body();
		JsonPath jsonPath = new JsonPath(resTW7.asString());
		accountID = jsonPath.getString("response.accountId");

		System.out.println("API no 8----------------------------------------");
		// Oauth ro token generation
		String accessTokenRO = getAccessTokenRO(email, "TWWebMyAcct_RO");
		System.out.println("RO oauth token ==>" + accessTokenRO);

		RequestSpecification reqTW9 = RestAssured.given();
		reqTW9.header("Authorization", "Bearer " + accessTokenRO);
		reqTW9.header("Content-Type", "application/json");
		reqTW9.header("X-Requested-With", "XMLHttpRequest");
		reqTW9.body(serviceOrder_PayloadTW(accountID, ESN).toString());
		Response resTW9 = reqTW9.and().post("/pep/order-mgmt/v2/serviceorder");
		System.out
				.println("API no 9---------------/pep/order-mgmt/v2/serviceorder   POST-Method API");
		resTW9.then().log().body();
		JsonPath jsonPath1 = new JsonPath(resTW9.asString());
		groupID = jsonPath1
				.getString("response.orderItems[\"0\"].orderItemExtension[8].value");

		RequestSpecification reqTW10 = RestAssured.given();
		reqTW10.header("Authorization", "Bearer " + accessTokenRO);
		reqTW10.header("Content-Type", "application/json");
		reqTW10.header("X-Requested-With", "XMLHttpRequest");
		reqTW10.body(estimate_TWPayload(accountID, ESN).toString());
		Response resTW10 = reqTW10.and().post(
				"/pep/order-mgmt/v1/productorder/estimate");
		System.out
				.println("API no 10---------------/pep/order-mgmt/v1/productorder/estimate   POST-Method API");
		resTW10.then().log().body();

		Response resTW11 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenRO)
				.when()
				.get("/pep/getAccountProfile/" + accountID + "?account_id="
						+ accountID + "&brandName=" + brandName + "&language="
						+ language + "&sourceSystem=" + sourceSystem
						+ "&timeStamp="
						+ String.format("%d", System.currentTimeMillis()));
		System.out
				.println("API no 11---------------/pep/getAccountProfile/   GET-Method API");
		resTW11.then().log().body();

		Response resTW12 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenRO)
				.when()
				.get("/pep/getPaymentSource/" + accountID + "?account_id="
						+ accountID + "&brandName=" + brandName + "&language="
						+ language + "&sourceSystem=" + sourceSystem
						+ "&timeStamp="
						+ String.format("%d", System.currentTimeMillis()));
		System.out
				.println("API no 12---------------/pep/getPaymentSource/   GET-Method API");
		resTW12.then().log().body();

		RequestSpecification reqTW13 = RestAssured.given();
		reqTW13.header("Authorization", "Bearer " + accessTokenRO);
		reqTW13.header("Content-Type", "application/json");
		reqTW13.header("X-Requested-With", "XMLHttpRequest");
		reqTW13.body(productOrder_PayLoadTW(accountID, ESN, SIM, groupID)
				.toString());
		Response resTW13 = reqTW13.and()
				.post("/pep/order-mgmt/v2/productorder");
		System.out
				.println("API no 13---------------/pep/order-mgmt/v2/productorder   POST-Method API");
		resTW13.then().log().body();

		Response resTW14 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenRO)
				.when()
				.get("/pep/getAccountDetails/v2/" + accountID + "?brandName="
						+ brandName + "&language=" + language
						+ "&sourceSystem=" + sourceSystem + "&timeStamp="
						+ String.format("%d", System.currentTimeMillis()));
		System.out
				.println("API no 14---------------/pep/getAccountProfile/   GET-Method API");
		resTW14.then().log().body();
	}

	@Test(groups = "simple_mobile")
	public void SimpleMobile_APIs() throws Exception {
		db.envSetup(env);
		db.setUp(dbProductName);
		String ESN = db.getEsn();
		// String pin = db.getPin();
		String SIM = db.getSim();
		String accountID, BillingID;

		// Oauth cc token generation
		String accessTokenCC = getAccessTokenCC("SMWebMyAcct_CCU");
		System.out.println("CC oauth token ==>" + accessTokenCC);

		Response resSM2 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenCC)
				.when()
				.get("/pep/servicePlanSelector/v2/?brandName=" + brandName
						+ "&language=" + language + "&sourceSystem="
						+ sourceSystem + "&timeStamp="
						+ String.format("%d", System.currentTimeMillis()));
		System.out
				.println("API no 2------------------/pep/servicePlanSelector/v2/   GET-Method API");
		resSM2.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(resSM2.getBody().jsonPath()
				.getString("status.type"), "SUCCESS",
				"API has failed in this step");

		Response resSM3 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenCC)
				.when()
				.get("/pep/validateUserDevice/v2/sim/" + SIM + "?brandName="
						+ brandName + "&language=" + language + "&min=" + SIM
						+ "&sourceSystem=" + sourceSystem + "&timeStamp="
						+ String.format("%d", System.currentTimeMillis()));
		System.out
				.println("API no 3------------------/pep/validateUserDevice/v2/sim/  GET-Method API");
		resSM3.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(resSM3.getBody().jsonPath()
				.getString("status.type"), "SUCCESS",
				"API has failed in this step");

		Response resSM4 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenCC)
				.when()
				.get("/pep/customer-mgmt/v1/customer-account/status?accountIdentifier="
						+ ESN
						+ "&brandName="
						+ brandName
						+ "&language="
						+ language
						+ "&sourceSystem="
						+ sourceSystem
						+ "&type=HANDSET&timeStamp="
						+ String.format("%d", System.currentTimeMillis()));
		System.out
				.println("API no 4------------------/pep/customer-mgmt/v1/customer-account/status  GET-Method API");
		resSM4.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(resSM4.getBody().jsonPath()
				.getString("status.type"), "SUCCESS",
				"API has failed in this step");

		RequestSpecification reqSM5 = RestAssured.given();
		reqSM5.header("Authorization", "Bearer " + accessTokenCC);
		reqSM5.header("Content-Type", "application/json");
		reqSM5.body(service_QualificationJSON_NTSM(ESN, SIM, "TECHNOLOGY",
				"SMWEB").toString());
		Response resSM5 = reqSM5
				.post("/pep/service-qualification-mgmt/v1/service-qualification");
		System.out
				.println("API no 5-----------------/pep/service-qualification-mgmt/v1/service-qualification \"Category - TECHNOLOGY\"   POST-Method API");
		resSM5.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(resSM5.getBody().jsonPath()
				.getString("status.type"), "SUCCESS",
				"API has failed in this step");

		RequestSpecification reqSM6 = RestAssured.given();
		reqSM6.header("Authorization", "Bearer " + accessTokenCC);
		reqSM6.header("Content-Type", "application/json");
		reqSM6.body(service_QualificationJSON_NTSM(ESN, SIM, "ACTIVATION",
				"SMWEB").toString());
		Response resSM6 = reqSM6
				.post("/pep/service-qualification-mgmt/v1/service-qualification");
		System.out
				.println("API no 6-----------------/pep/service-qualification-mgmt/v1/service-qualification \"Category - TECHNOLOGY\"   POST-Method API");
		resSM6.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(resSM6.getBody().jsonPath()
				.getString("status.type"), "SUCCESS",
				"API has failed in this step");

		RequestSpecification reqSM7 = RestAssured.given();
		reqSM7.header("Authorization", "Bearer " + accessTokenCC);
		reqSM7.header("Content-Type", "application/json");
		reqSM7.header("X-Requested-With", "XMLHttpRequest");
		reqSM7.body(createAccountJson(ESN, email).toString());
		Response resSM7 = reqSM7.and().post(
				"/pep/createNewAccount/".concat(email));
		System.out
				.println("API no 7---------------/pep/createNewAccount/   POST-Method API");
		resSM7.then().log().status().and().assertThat().statusCode(200);
		JsonPath jsonPath = new JsonPath(resSM7.asString());
		accountID = jsonPath.getString("response.accountId");
		Assert.assertEquals(resSM7.getBody().jsonPath()
				.getString("status.type"), "SUCCESS",
				"API has failed in this step");

		System.out.println("API no 8----------------------------------------");
		// Oauth ro token generation
		String accessTokenRO = getAccessTokenRO(email, "SMWebMyAcct_RO");
		System.out.println("RO oauth token ==>" + accessTokenRO);

		RequestSpecification reqSM9 = RestAssured.given();
		reqSM9.header("Authorization", "Bearer " + accessTokenRO);
		reqSM9.header("Content-Type", "application/json");
		reqSM9.header("X-Requested-With", "XMLHttpRequest");
		reqSM9.body(estimate_SMPayload(accountID, ESN).toString());
		Response resSM9 = reqSM9.and().post(
				"/pep/order-mgmt/v1/productorder/estimate");
		System.out
				.println("API no 9---------------/pep/order-mgmt/v1/productorder/estimate   POST-Method API");
		resSM9.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(resSM9.getBody().jsonPath()
				.getString("status.type"), "SUCCESS",
				"API has failed in this step");

		Response resSM10 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenRO)
				.when()
				.get("/pep/getAccountProfile/" + accountID + "?account_id="
						+ accountID + "&brandName=" + brandName + "&language="
						+ language + "&sourceSystem=" + sourceSystem
						+ "&timeStamp="
						+ String.format("%d", System.currentTimeMillis()));
		System.out
				.println("API no 10---------------/pep/getAccountProfile/   GET-Method API");
		resSM10.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(
				resSM10.getBody().jsonPath().getString("status.type"),
				"SUCCESS", "API has failed in this step");

		Response resSM11 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenRO)
				.when()
				.get("/pep/getPaymentSource/" + accountID + "?account_id="
						+ accountID + "&brandName=" + brandName + "&language="
						+ language + "&sourceSystem=" + sourceSystem
						+ "&timeStamp="
						+ String.format("%d", System.currentTimeMillis()));
		System.out
				.println("API no 11---------------/pep/getPaymentSource/   GET-Method API");
		resSM11.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(
				resSM11.getBody().jsonPath().getString("status.type"),
				"FAILURE", "API has failed in this step");

		RequestSpecification reqSM12 = RestAssured.given();
		reqSM12.header("Authorization", "Bearer " + accessTokenRO);
		reqSM12.header("Content-Type", "application/json");
		reqSM12.header("X-Requested-With", "XMLHttpRequest");
		reqSM12.body(productOrder_PayloadSM(accountID, ESN, SIM).toString());
		Response resSM12 = reqSM12.and()
				.post("/pep/order-mgmt/v2/productorder");
		System.out
				.println("API no 12---------------/pep/order-mgmt/v2/productorder   POST-Method API");
		resSM12.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(
				resSM12.getBody().jsonPath().getString("status.type"),
				"SUCCESS", "API has failed in this step");

		Response resSM13 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenRO)
				.when()
				.get("/pep/getAccountDetails/v2/" + accountID + "?brandName="
						+ brandName + "&language=" + language
						+ "&sourceSystem=" + sourceSystem + "&timeStamp="
						+ String.format("%d", System.currentTimeMillis()));
		System.out
				.println("API no 13---------------/pep/getAccountDetails/v2/   GET-Method API");
		resSM13.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(
				resSM13.getBody().jsonPath().getString("status.type"),
				"SUCCESS", "API has failed in this step");

		Response resSM14 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenRO)
				.when()
				.get("/pep/customer-mgmt/v1/customer-account/status?accountIdentifier="
						+ accountID
						+ "&brandName="
						+ brandName
						+ "&language="
						+ language
						+ "&sourceSystem="
						+ sourceSystem
						+ "&type=accountID&timeStamp="
						+ String.format("%d", System.currentTimeMillis()));
		System.out
				.println("API no 14------------------/pep/customer-mgmt/v1/customer-account/status  GET-Method API");
		resSM14.then().log().status().and().assertThat().statusCode(200);
		JsonPath jp = new JsonPath(resSM14.asString());
		BillingID = jp.getString("response.billingAccount.id");
		Assert.assertEquals(
				resSM14.getBody().jsonPath().getString("status.type"),
				"SUCCESS", "API has failed in this step");

		Response resSM15 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenRO)
				.when()
				.get("/pep/billing-mgmt/v1/billing-account/balance?brandName="
						+ brandName + "&category=BAN&identifier=" + BillingID
						+ "&language=" + language
						+ "&lineInquiry=false&sourceSystem=" + sourceSystem
						+ "&zipcode=33178&timeStamp="
						+ String.format("%d", System.currentTimeMillis()));
		System.out
				.println("API no 15------------------/pep/billing-mgmt/v1/billing-account/balance  GET-Method API");
		resSM15.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(
				resSM15.getBody().jsonPath().getString("status.type"),
				"SUCCESS", "API has failed in this step");

		Response resSM16 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenRO)
				.when()
				.get("/pep/getScript/scripts/STR_8158?brandName="
						+ brandName
						+ "&language="
						+ language
						+ "&partClass=SMZEZ828TL&script_id=STR_8158&sourceSystem="
						+ sourceSystem + "&timeStamp="
						+ String.format("%d", System.currentTimeMillis()));
		System.out
				.println("API no 16---------------/pep/getScript/scripts/STR_8158   GET-Method API");
		resSM16.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(
				resSM16.getBody().jsonPath().getString("status.type"),
				"SUCCESS", "API has failed in this step");
	}

	@Test(groups = "wfm")
	public void MyFamilyMobile_APIs() throws Exception {
		db.envSetup(env);
		db.setUp(dbProductName);
		String ESN = db.getEsn();
		// String pin = db.getPin();
		String SIM = db.getSim();
		String accountID, BillingID;

		// Oauth cc token generation
		String accessTokenCC = getAccessTokenCC("WFMWebMyAcct_CCU");
		System.out.println("CC oauth token ==>" + accessTokenCC);

		Response resWFM2 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenCC)
				.when()
				.get("/pep/servicePlanSelector/v2/?brandName=" + brandName
						+ "&language=" + language + "&sourceSystem="
						+ sourceSystem + "&timeStamp="
						+ String.format("%d", System.currentTimeMillis()));
		System.out
				.println("API no 2------------------/pep/servicePlanSelector/v2/   GET-Method API");
		resWFM2.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(
				resWFM2.getBody().jsonPath().getString("status.type"),
				"SUCCESS", "API has failed in this step");

		RequestSpecification reqWFM3 = RestAssured.given();
		reqWFM3.header("Authorization", "Bearer " + accessTokenCC);
		reqWFM3.header("Content-Type", "application/json");
		reqWFM3.header("X-Requested-With", "XMLHttpRequest");
		reqWFM3.body(WFM_Version().toString());
		Response resWFM3 = reqWFM3.and().post("/pep/version");
		System.out
				.println("API no 3---------------/pep/version   POST-Method API");
		resWFM3.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(
				resWFM3.getBody().jsonPath().getString("status.type"),
				"SUCCESS", "API has failed in this step");

		RequestSpecification reqWFM4 = RestAssured.given();
		reqWFM4.header("Authorization", "Bearer " + accessTokenCC);
		reqWFM4.header("Content-Type", "application/json");
		reqWFM4.body(service_QualificationJSON_WFM(ESN, SIM, "ACH_ELIGIBILITY")
				.toString());
		Response resWFM4 = reqWFM4
				.post("/pep/service-qualification-mgmt/v1/service-qualification");
		System.out
				.println("API no 4----------------/pep/service-qualification-mgmt/v1/service-qualification \"Category - ACH_ELIGIBILITY\"   POST-Method API");
		resWFM4.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(
				resWFM4.getBody().jsonPath().getString("status.type"),
				"SUCCESS", "API has failed in this step");

		Response resWFM5 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenCC)
				.when()
				.get("/pep/validateUserDevice/v2/sim/" + SIM + "?brandName="
						+ brandName + "&language=" + language + "&min=" + SIM
						+ "&sourceSystem=" + sourceSystem + "&timeStamp="
						+ String.format("%d", System.currentTimeMillis()));
		System.out
				.println("API no 5------------------/pep/validateUserDevice/v2/sim/  GET-Method API");
		resWFM5.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(
				resWFM5.getBody().jsonPath().getString("status.type"),
				"SUCCESS", "API has failed in this step");

		Response resWFM6 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenCC)
				.when()
				.get("/pep/accountRecovery/v2/accountStatus/" + ESN
						+ "?brandName=" + brandName + "&device=" + ESN
						+ "&language=" + language + "&sourceSystem="
						+ sourceSystem + "&timeStamp="
						+ String.format("%d", System.currentTimeMillis()));
		System.out
				.println("API no 6------------------/pep/accountRecovery/v2/accountStatus/  GET-Method API");
		resWFM6.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(
				resWFM6.getBody().jsonPath().getString("status.type"),
				"SUCCESS", "API has failed in this step");

		RequestSpecification reqWFM7 = RestAssured.given();
		reqWFM7.header("Authorization", "Bearer " + accessTokenCC);
		reqWFM7.header("Content-Type", "application/json");
		reqWFM7.body(service_QualificationJSON_WFM(ESN, SIM, "TECHNOLOGY")
				.toString());
		Response resWFM7 = reqWFM7
				.post("/pep/service-qualification-mgmt/v1/service-qualification");
		System.out
				.println("API no 7----------------/pep/service-qualification-mgmt/v1/service-qualification \"Category - TECHNOLOGY\"   POST-Method API");
		resWFM7.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(
				resWFM7.getBody().jsonPath().getString("status.type"),
				"SUCCESS", "API has failed in this step");

		RequestSpecification reqWFM8 = RestAssured.given();
		reqWFM8.header("Authorization", "Bearer " + accessTokenCC);
		reqWFM8.header("Content-Type", "application/json");
		reqWFM8.body(service_QualificationJSON_WFM(ESN, SIM, "ACTIVATION")
				.toString());
		Response resWFM8 = reqWFM8
				.post("/pep/service-qualification-mgmt/v1/service-qualification");
		System.out
				.println("API no 8----------------/pep/service-qualification-mgmt/v1/service-qualification \"Category - ACTIVATION\"   POST-Method API");
		resWFM8.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(
				resWFM8.getBody().jsonPath().getString("status.type"),
				"SUCCESS", "API has failed in this step");

		RequestSpecification reqWFM9 = RestAssured.given();
		reqWFM9.header("Authorization", "Bearer " + accessTokenCC);
		reqWFM9.header("Content-Type", "application/json");
		reqWFM9.header("X-Requested-With", "XMLHttpRequest");
		reqWFM9.body(createAccountJson(ESN, email).toString());
		Response resWFM9 = reqWFM9.and().post(
				"/pep/createNewAccount/".concat(email));
		System.out
				.println("API no 9---------------/pep/createNewAccount/   POST-Method API");
		resWFM9.then().log().status().and().assertThat().statusCode(200);
		JsonPath jsonPath = new JsonPath(resWFM9.asString());
		accountID = jsonPath.getString("response.accountId");
		Assert.assertEquals(
				resWFM9.getBody().jsonPath().getString("status.type"),
				"SUCCESS", "API has failed in this step");

		System.out.println("API no 10--------------/oauth/ro     Oauth_API");
		// Oauth ro token generation
		String accessTokenRO = getAccessTokenRO(email, "WFMWebMyAcct_RO");
		System.out.println("RO oauth token ==>" + accessTokenRO);

		Response resWFM11 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenRO)
				.when()
				.get("/pep/getAccountProfile/" + accountID + "?account_id="
						+ accountID + "&brandName=" + brandName + "&language="
						+ language + "&sourceSystem=" + sourceSystem
						+ "&timeStamp="
						+ String.format("%d", System.currentTimeMillis()));
		System.out
				.println("API no 11---------------/pep/getAccountProfile/   GET-Method API");
		resWFM11.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(
				resWFM11.getBody().jsonPath().getString("status.type"),
				"SUCCESS", "API has failed in this step");

		RequestSpecification reqWFM12 = RestAssured.given();
		reqWFM12.header("Authorization", "Bearer " + accessTokenRO);
		reqWFM12.header("Content-Type", "application/json");
		reqWFM12.header("X-Requested-With", "XMLHttpRequest");
		reqWFM12.body(estimate_WFMPayload(accountID, ESN).toString());
		Response resWFM12 = reqWFM12.and().post(
				"/pep/order-mgmt/v1/productorder/estimate");
		System.out
				.println("API no 12---------------/pep/order-mgmt/v1/productorder/estimate   POST-Method API");
		resWFM12.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(
				resWFM12.getBody().jsonPath().getString("status.type"),
				"SUCCESS", "API has failed in this step");

		Response resWFM13 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenRO)
				.when()
				.get("/pep/getPaymentSource/" + accountID + "?account_id="
						+ accountID + "&brandName=" + brandName + "&language="
						+ language + "&sourceSystem=" + sourceSystem
						+ "&timeStamp="
						+ String.format("%d", System.currentTimeMillis()));
		System.out
				.println("API no 13---------------/pep/getPaymentSource/   GET-Method API");
		resWFM13.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(
				resWFM13.getBody().jsonPath().getString("status.type"),
				"FAILURE", "API has failed in this step");

		RequestSpecification reqWFM14 = RestAssured.given();
		reqWFM14.header("Authorization", "Bearer " + accessTokenRO);
		reqWFM14.header("Content-Type", "application/json");
		reqWFM14.header("X-Requested-With", "XMLHttpRequest");
		reqWFM14.body(productOrder_PayloadWFM(accountID, ESN, SIM).toString());
		Response resWFM14 = reqWFM14.and().post(
				"/pep/order-mgmt/v2/productorder");
		System.out
				.println("API no 14---------------/pep/order-mgmt/v2/productorder   POST-Method API");
		resWFM14.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(
				resWFM14.getBody().jsonPath().getString("status.type"),
				"SUCCESS", "API has failed in this step");

		Response resWFM15 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenRO)
				.when()
				.get("/pep/getAccountDetails/v2/" + accountID + "?brandName="
						+ brandName + "&language=" + language
						+ "&sourceSystem=" + sourceSystem + "&timeStamp="
						+ String.format("%d", System.currentTimeMillis()));
		System.out
				.println("API no 15---------------/pep/getAccountDetails/v2/   GET-Method API");
		resWFM15.then().log().status().and().assertThat().statusCode(200);
		JsonPath jp = new JsonPath(resWFM15.asString());
		BillingID = jp.getString("response.billingAccount.id");
		Assert.assertEquals(
				resWFM15.getBody().jsonPath().getString("status.type"),
				"SUCCESS", "API has failed in this step");

		Response resWFM16 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenRO)
				.when()
				.get("/pep/billing-mgmt/v1/billing-account/balance?brandName="
						+ brandName + "&category=BAN&identifier=" + BillingID
						+ "&language=" + language
						+ "&lineInquiry=false&sourceSystem=" + sourceSystem
						+ "&zipcode=33178&timeStamp="
						+ String.format("%d", System.currentTimeMillis()));
		System.out
				.println("API no 16------------------/pep/billing-mgmt/v1/billing-account/balance  GET-Method API");
		resWFM16.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(
				resWFM16.getBody().jsonPath().getString("status.type"),
				"SUCCESS", "API has failed in this step");

		Response resWFM17 = RestAssured
				.given()
				.header("Authorization", "Bearer " + accessTokenRO)
				.when()
				.get("/pep/getScript/scripts/STR_8158?brandName="
						+ brandName
						+ "&language="
						+ language
						+ "&partClass=WFMAL4060WL&script_id=STR_8158&sourceSystem="
						+ sourceSystem + "&timeStamp="
						+ String.format("%d", System.currentTimeMillis()));
		System.out
				.println("API no 17---------------/pep/getScript/scripts/STR_8158   GET-Method API");
		resWFM17.then().log().status().and().assertThat().statusCode(200);
		Assert.assertEquals(
				resWFM17.getBody().jsonPath().getString("status.type"),
				"SUCCESS", "API has failed in this step");

	}
}
