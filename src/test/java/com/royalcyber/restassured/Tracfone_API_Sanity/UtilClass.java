package com.royalcyber.restassured.Tracfone_API_Sanity;

/**
 * @author Arunkumar Ravichandran
 *
 */

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class UtilClass {

	protected String responseDescription = "Success";

	public String randomGenerator() {
		String possibleChars = "abcdefghijklmnopqrstuvwxyz";
		StringBuilder strFinal = new StringBuilder();
		Random rnd = new Random();
		while (strFinal.length() < 4) {
			int index = (int) (rnd.nextFloat() * possibleChars.length());
			strFinal.append(possibleChars.charAt(index));
		}
		return strFinal.toString();
	}

	public String getDateandTime() {
		Date now = new Date();
		SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss Z");
		return ft.format(now);
	}

}
