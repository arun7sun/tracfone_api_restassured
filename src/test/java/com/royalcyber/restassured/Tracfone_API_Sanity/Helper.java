package com.royalcyber.restassured.Tracfone_API_Sanity;

/**
 * @author Arunkumar Ravichandran
 *
 */

import java.util.HashMap;
import java.util.Map;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.specification.RequestSpecification;

import org.json.*;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

public class Helper extends UtilClass {
	protected String env = "";
	protected String brandName = "";
	protected String dbProductName = "";
	protected String sourceSystem = "WEB";
	protected String language = "ENG";
	protected String partyID = "TFAPP-ST";
	private String zipcode = "33178";
	private String ClientId;
	private String ClientSecret = "abc123**";
	private String GrantType;
	private String scope;
	protected String email = randomGenerator().concat("@tf.com");

	@Parameters({ "Env", "Product" })
	@BeforeSuite(alwaysRun = true)
	public void configAPI(String env, String product) {
		RestAssured.baseURI = "https://" + env + "apifull.tracfone.com";
		this.brandName = product.toUpperCase();
		this.dbProductName = product;
		this.env = env;
	}

	public RequestSpecification getRequestprerequisite() {
		return RestAssured.given().contentType(ContentType.JSON);
	}

	protected String getAccessTokenCC(String clientID) {
		this.GrantType = "client_credentials";
		this.ClientId = clientID;
		this.scope = "createNewAccount";
		String response = RestAssured.given()
				.params("grant_type", GrantType, "scope", scope).auth()
				.preemptive().basic(this.ClientId, ClientSecret).when()
				.post("/oauth/cc").asString();
		JsonPath jsonPath = new JsonPath(response);
		String token = jsonPath.getString("access_token");
		return token;
	}

	protected String getAccessTokenRO(String UserName, String clientID) {
		this.GrantType = "password";
		this.ClientId = clientID;
		this.scope = "getSecurityQuestions";
		String response = RestAssured
				.given()
				.parameters("username", UserName, "password", "passw0rd",
						"client_id", this.ClientId, "client_secret",
						ClientSecret, "grant_type", GrantType, "scope", scope,
						"sourceSystem", sourceSystem, "brandName", brandName)
				.auth().preemptive().basic(UserName, "passw0rd").when()
				.post("oauth/ro").asString();
		JsonPath jsonPath = new JsonPath(response);
		String token_ro = jsonPath.getString("access_token");
		return token_ro;
	}

	protected JSONObject service_QualificationJSON(String esn, String sim,
			String serv_Category, String roletype) {
		JSONObject finalJson = new JSONObject();
		JSONObject commonSub = new JSONObject();
		JSONObject requestSub = new JSONObject();

		JSONArray relatedParties = new JSONArray();
		JSONArray serviceCategory = new JSONArray();
		JSONObject serviceSpecification = new JSONObject();
		JSONObject location = new JSONObject();
		JSONArray resources = new JSONArray();
		JSONObject scObject = new JSONObject();
		JSONObject rpObject = new JSONObject();
		JSONObject party = new JSONObject();
		JSONArray partyExtension = new JSONArray();
		JSONObject partyExtension_E1 = new JSONObject();
		JSONObject partyExtension_E2 = new JSONObject();
		JSONObject handsetResource = new JSONObject();
		JSONObject simResource = new JSONObject();
		JSONObject postalAddress = new JSONObject();

		commonSub.putOnce("brandName", brandName);
		commonSub.putOnce("sourceSystem", sourceSystem);
		commonSub.putOnce("language", language);
		if (brandName != "TRACFONE") {
			commonSub.putOnce("subBrandName", brandName);
		}

		partyExtension_E1.put("name", "partyTransactionID");
		partyExtension_E1.put("value", "web_1524814655055");
		partyExtension_E2.put("name", "sourceSystem");
		partyExtension_E2.put("value", sourceSystem);
		partyExtension.put(partyExtension_E1);
		partyExtension.put(partyExtension_E2);
		party.put("partyID", partyID);
		party.put("languageAbility", language);
		party.put("partyExtension", partyExtension);
		rpObject.put("roleType", roletype);
		rpObject.put("party", party);
		relatedParties.put(rpObject);

		postalAddress.put("zipcode", zipcode);
		location.put("postalAddress", postalAddress);

		scObject.put("type", "context");
		scObject.put("value", serv_Category);
		serviceCategory.put(scObject);

		serviceSpecification.put("brand", brandName);

		handsetResource.put("serialNumber", esn);
		handsetResource.put("name", "productSerialNumber");
		handsetResource.put("type", "HANDSET");
		simResource.put("serialNumber", sim);
		simResource.put("name", "serialNumber");
		simResource.put("type", "SIM_CARD");
		resources.put(handsetResource);
		resources.put(simResource);

		requestSub.put("relatedParties", relatedParties);
		requestSub.put("location", location);
		requestSub.put("serviceCategory", serviceCategory);
		requestSub.put("serviceSpecification", serviceSpecification);
		requestSub.put("resources", resources);

		finalJson.put("common", commonSub);
		finalJson.put("request", requestSub);
		return finalJson;
	}

	protected JSONObject createAccountJson(String esn, String mail) {
		JSONObject finalJson = new JSONObject();
		JSONObject commonSub = new JSONObject();
		JSONObject requestSub = new JSONObject();
		JSONObject commPreferences = new JSONObject();

		commonSub.putOnce("brandName", brandName);
		commonSub.putOnce("sourceSystem", sourceSystem);
		commonSub.putOnce("language", language);

		requestSub.put("accountId", 0);
		requestSub.put("esn", esn);
		requestSub.put("password", "passw0rd");
		requestSub.put("securityCode", "1234");
		requestSub.put("dateOfBirth", "11-23-1995");
		requestSub.put("zipcode", zipcode);
		commPreferences.put("offers", true);
		commPreferences.put("savings", true);
		requestSub.put("commPreferences", commPreferences);

		finalJson.put("common", commonSub);
		finalJson.put("request", requestSub);
		finalJson.put("email", mail);
		return finalJson;
	}

	protected JSONObject calculateTax_Payload(String esn, int accID) {
		JSONObject finalJson = new JSONObject();
		JSONObject commonSub = new JSONObject();
		JSONObject requestSub = new JSONObject();

		JSONArray cart = new JSONArray();
		JSONObject product1 = new JSONObject();

		commonSub.putOnce("brandName", brandName);
		commonSub.putOnce("sourceSystem", sourceSystem);
		commonSub.putOnce("language", language);
		requestSub.put("accountId", accID);
		requestSub.put("esn", esn);
		requestSub.put("billingZipCode", zipcode);
		product1.put("quantity", 1);
		product1.put("partNumber", "TFSAPP1SPH0050");
		cart.put(product1);
		requestSub.put("cart", cart);

		finalJson.put("common", commonSub);
		finalJson.put("request", requestSub);
		return finalJson;
	}

	protected JSONObject WFM_Version() {
		JSONObject finalJson = new JSONObject();
		JSONObject commonSub = new JSONObject();
		commonSub.putOnce("brandName", brandName);
		commonSub.putOnce("sourceSystem", sourceSystem);
		commonSub.putOnce("language", language);
		finalJson.put("common", commonSub);
		return finalJson;
	}

	protected JSONObject productOrder_PayLoadTF(String accID, String esn,
			String sim) {
		JSONObject finalJson = new JSONObject();
		JSONObject commonSub = new JSONObject();
		JSONObject requestSub = new JSONObject();

		JSONArray relatedParties = new JSONArray();
		JSONObject rpObject_1 = new JSONObject();
		JSONObject rpObject_2 = new JSONObject();
		JSONObject party1 = new JSONObject();
		JSONObject party2 = new JSONObject();
		JSONObject individual = new JSONObject();
		JSONArray partyExtension = new JSONArray();
		JSONObject partyExtension_E1 = new JSONObject();
		JSONObject partyExtension_E2 = new JSONObject();
		JSONArray orderItems = new JSONArray();
		JSONObject oiObject = new JSONObject();
		JSONObject productOffering = new JSONObject();
		JSONObject productSpecification = new JSONObject();
		JSONObject product = new JSONObject();
		JSONArray relatedServices = new JSONArray();
		JSONObject rsObject = new JSONObject();
		JSONArray supportingResources = new JSONArray();
		JSONObject srObject = new JSONObject();
		JSONObject location = new JSONObject();
		JSONObject postalAddress = new JSONObject();
		JSONObject billingAccount = new JSONObject();
		JSONObject paymentPlan = new JSONObject();
		JSONObject paymentMean = new JSONObject();
		JSONObject creditCard = new JSONObject();
		JSONObject billingAddress = new JSONObject();

		individual.put("id", accID);
		party1.put("individual", individual);
		partyExtension_E1.put("name", "partyTransactionID");
		partyExtension_E1.put("value", "web_1525768512768");
		partyExtension_E2.put("name", "sourceSystem");
		partyExtension_E2.put("value", sourceSystem);
		partyExtension.put(partyExtension_E1);
		partyExtension.put(partyExtension_E2);
		party2.put("languageAbility", language);
		party2.put("partyExtension", partyExtension);
		rpObject_1.put("party", party1);
		rpObject_1.put("roleType", "customer");
		rpObject_2.put("party", party2);
		rpObject_2.put("roleType", "application");
		relatedParties.put(rpObject_1);
		relatedParties.put(rpObject_2);

		oiObject.put("id", "1");
		oiObject.put("quantity", "1");
		oiObject.put("action", "ACTIVATION");
		productSpecification.put("brand", brandName);
		productOffering.put("id", "TFSAPP1SPH0050");
		productOffering.put("name", "");
		productOffering.put("productSpecification", productSpecification);
		oiObject.put("productOffering", productOffering);
		rsObject.put("id", "1342378703");
		rsObject.put("category", "SERVICE_PLAN");
		rsObject.put("isRedeemNow", true);
		relatedServices.put(rsObject);
		srObject.put("resourceType", "SIM_CARD");
		srObject.put("serialNumber", sim);
		supportingResources.put(srObject);
		product.put("productSpecification", productSpecification);
		product.put("relatedServices", relatedServices);
		product.put("productSerialNumber", esn);
		product.put("productCategory", "HANDSET");
		product.put("supportingResources", supportingResources);
		oiObject.put("product", product);
		postalAddress.put("zipcode", zipcode);
		location.put("postalAddress", postalAddress);
		oiObject.put("location", location);
		orderItems.put(oiObject);

		creditCard.put("year", "2020");
		creditCard.put("month", "11");
		creditCard.put("cvv", "234");
		creditCard.put("accountNumber", "4556941005662411");
		creditCard.put("type", "VISA");
		billingAddress.put("addressLine1", "no-1234");
		billingAddress.put("addressLine2", "charleston road");
		billingAddress.put("city", "Mountain View");
		billingAddress.put("stateOrProvince", "CA");
		billingAddress.put("zipCode", "94043");
		billingAddress.put("country", "USA");
		paymentMean.put("isDefault", false);
		paymentMean.put("savePaymentInfo", false);
		paymentMean.put("type", "CREDITCARD");
		paymentMean.put("accountHolderName", "cyber source");
		paymentMean.put("firstName", "cyber");
		paymentMean.put("lastName", "source");
		paymentMean.put("creditCard", creditCard);
		paymentMean.put("billingAddress", billingAddress);
		paymentPlan.put("isRecurring", false);
		paymentPlan.put("paymentMean", paymentMean);
		billingAccount.put("paymentPlan", paymentPlan);

		commonSub.putOnce("brandName", brandName);
		commonSub.putOnce("sourceSystem", sourceSystem);
		commonSub.putOnce("language", language);
		requestSub.put("externalID", "123456");
		requestSub.put("orderDate", getDateandTime());
		requestSub.put("relatedParties", relatedParties);
		requestSub.put("orderItems", orderItems);
		requestSub.put("billingAccount", billingAccount);
		finalJson.put("common", commonSub);
		finalJson.put("request", requestSub);
		return finalJson;
	}

	protected Map<String, String> queryParam_ProductOffering(String esn) {
		Map<String, String> criteria = new HashMap<String, String>();
		criteria.put("planCategory", "VAS");
		criteria.put("planSubCategory", "INSURANCE");
		criteria.put("deviceType", "SMARTPHONE");
		criteria.put("zipCode", zipcode);
		criteria.put("esn", esn);
		return criteria;
	}

	protected JSONObject estimate_NTPayload(String accID, String esn) {
		JSONObject finalJson = new JSONObject();
		JSONObject commonSub = new JSONObject();
		JSONObject requestSub = new JSONObject();
		JSONArray relatedParties = new JSONArray();
		JSONObject rpObject1 = new JSONObject();
		JSONObject rpObject2 = new JSONObject();
		JSONObject party1 = new JSONObject();
		JSONObject individual = new JSONObject();
		JSONObject party2 = new JSONObject();
		JSONArray partyExtension = new JSONArray();
		JSONObject PE_Object1 = new JSONObject();
		JSONObject PE_Object2 = new JSONObject();
		JSONArray orderItems = new JSONArray();
		JSONObject oiObject = new JSONObject();
		JSONObject product = new JSONObject();
		JSONArray relatedServices = new JSONArray();
		JSONObject rsObject = new JSONObject();
		JSONArray characteristicSpecification = new JSONArray();
		JSONObject csObject = new JSONObject();
		JSONObject productOffering = new JSONObject();
		JSONObject productSpecification = new JSONObject();
		JSONArray location = new JSONArray();
		JSONObject locObject = new JSONObject();
		JSONObject address = new JSONObject();

		commonSub.putOnce("brandName", brandName);
		commonSub.putOnce("sourceSystem", sourceSystem);
		commonSub.putOnce("language", language);
		commonSub.putOnce("subBrandName", brandName);

		individual.put("id", accID);
		party1.put("individual", individual);
		rpObject1.put("party", party1);
		rpObject1.put("roleType", "customer");
		PE_Object1.put("name", "partyTransactionID");
		PE_Object1.put("value", "web_1231234234424");
		PE_Object2.put("name", "sourceSystem");
		PE_Object2.put("value", sourceSystem);
		partyExtension.put(PE_Object1);
		partyExtension.put(PE_Object2);
		party2.put("partyID", "NET10WEB");
		party2.put("languageAbility", language);
		party2.put("partyExtension", partyExtension);
		rpObject2.put("party", party2);
		rpObject2.put("roleType", "internal");
		relatedParties.put(rpObject1);
		relatedParties.put(rpObject2);

		csObject.put("name", "relatedProgramId");
		csObject.put("value", "5801742");
		characteristicSpecification.put(csObject);
		rsObject.put("id", "189");
		rsObject.put("category", "SERVICE_PLAN");
		rsObject.put("isRecurring", false);
		rsObject.put("characteristicSpecification", characteristicSpecification);
		relatedServices.put(rsObject);
		product.put("productSerialNumber", esn);
		product.put("productCategory", "HANDSET");
		product.put("relatedServices", relatedServices);

		productSpecification.put("id", "NT060D0600U");
		productSpecification.put("brand", brandName);
		productOffering.put("id", "NTAPP30600");
		productOffering.put("productSpecification", productSpecification);

		oiObject.put("id", "1");
		oiObject.put("action", "ESTIMATE");
		oiObject.put("quantity", 1);
		oiObject.put("product", product);
		oiObject.put("productOffering", productOffering);
		orderItems.put(oiObject);

		address.put("zipcode", zipcode);
		locObject.put("addressType", "BILLING");
		locObject.put("address", address);
		location.put(locObject);

		requestSub.put("relatedParties", relatedParties);
		requestSub.put("orderDate", getDateandTime());
		requestSub.put("orderItems", orderItems);
		requestSub.put("location", location);

		finalJson.put("common", commonSub);
		finalJson.put("request", requestSub);
		return finalJson;
	}

	protected JSONObject productOrder_PayloadNT(String accID, String esn,
			String sim) {
		JSONObject finalJson = new JSONObject();
		JSONObject commonSub = new JSONObject();
		JSONObject requestSub = new JSONObject();
		JSONArray relatedParties = new JSONArray();
		JSONObject rpObject_1 = new JSONObject();
		JSONObject rpObject_2 = new JSONObject();
		JSONObject rpObject_3 = new JSONObject();
		JSONObject party1 = new JSONObject();
		JSONObject party2 = new JSONObject();
		JSONObject party3 = new JSONObject();
		JSONObject individual = new JSONObject();
		JSONArray partyExtension = new JSONArray();
		JSONObject partyExtension_E1 = new JSONObject();
		JSONObject partyExtension_E2 = new JSONObject();
		JSONArray orderItems = new JSONArray();
		JSONObject oiObject = new JSONObject();
		JSONObject productOffering = new JSONObject();
		JSONObject productSpecification1 = new JSONObject();
		JSONObject productSpecification2 = new JSONObject();
		JSONArray supportingResources = new JSONArray();
		JSONObject srObject = new JSONObject();
		JSONObject product = new JSONObject();
		JSONArray relatedServices = new JSONArray();
		JSONObject rsObject = new JSONObject();
		JSONObject location = new JSONObject();
		JSONObject postalAddress = new JSONObject();
		JSONObject billingAccount = new JSONObject();
		JSONObject paymentPlan = new JSONObject();
		JSONObject paymentMean = new JSONObject();
		JSONObject creditCard = new JSONObject();
		JSONObject billingAddress = new JSONObject();
		JSONArray characteristicSpecification = new JSONArray();
		JSONObject csObject = new JSONObject();

		individual.put("id", accID);
		party1.put("individual", individual);
		party1.put("partyID", "CUST_HASH");
		partyExtension_E1.put("name", "partyTransactionID");
		partyExtension_E1.put("value", "web_1231234234424");
		partyExtension_E2.put("name", "sourceSystem");
		partyExtension_E2.put("value", sourceSystem);
		partyExtension.put(partyExtension_E1);
		partyExtension.put(partyExtension_E2);
		party2.put("partyID", "NET10WEB");
		party2.put("languageAbility", language);
		party2.put("partyExtension", partyExtension);
		party3.put("partyID", "");
		rpObject_1.put("party", party1);
		rpObject_1.put("roleType", "customer");
		rpObject_2.put("party", party2);
		rpObject_2.put("roleType", "internal");
		rpObject_3.put("party", party3);
		rpObject_2.put("roleType", "referrer");
		relatedParties.put(rpObject_1);
		relatedParties.put(rpObject_2);
		relatedParties.put(rpObject_3);

		oiObject.put("id", "1");
		oiObject.put("quantity", "1");
		oiObject.put("action", "ACTIVATION");
		productOffering.put("id", "NTAPP30600");
		productSpecification1.put("brand", brandName);
		productSpecification1.put("id", "NT060D0600U");
		productOffering.put("productSpecification", productSpecification1);
		oiObject.put("productOffering", productOffering);
		csObject.put("name", "relatedProgramId");
		csObject.put("value", "5801742");
		characteristicSpecification.put(csObject);
		rsObject.put("id", "189");
		rsObject.put("category", "SERVICE_PLAN");
		rsObject.put("isRecurring", true);
		rsObject.put("characteristicSpecification", characteristicSpecification);
		relatedServices.put(rsObject);
		productSpecification2.put("brand", brandName);
		product.put("productSpecification", productSpecification2);
		srObject.put("resourceType", "SIM_CARD");
		srObject.put("serialNumber", sim);
		supportingResources.put(srObject);
		product.put("relatedServices", relatedServices);
		product.put("supportingResources", supportingResources);
		product.put("productSerialNumber", esn);
		product.put("productCategory", "HANDSET");
		oiObject.put("product", product);
		postalAddress.put("zipcode", zipcode);
		location.put("postalAddress", postalAddress);
		oiObject.put("location", location);
		orderItems.put(oiObject);

		creditCard.put("year", "2020");
		creditCard.put("month", "11");
		creditCard.put("cvv", "234");
		creditCard.put("accountNumber", "4556941005662411");
		creditCard.put("type", "VISA");
		billingAddress.put("addressLine1", "no-1234");
		billingAddress.put("addressLine2", "charleston road");
		billingAddress.put("city", "Miami");
		billingAddress.put("stateOrProvince", "FL");
		billingAddress.put("zipCode", zipcode);
		billingAddress.put("country", "USA");
		paymentMean.put("isDefault", false);
		paymentMean.put("savePaymentInfo", true);
		paymentMean.put("type", "CREDITCARD");
		paymentMean.put("accountHolderName", "Cyber Source");
		paymentMean.put("firstName", "Cyber");
		paymentMean.put("lastName", "Source");
		paymentMean.put("creditCard", creditCard);
		paymentMean.put("billingAddress", billingAddress);
		paymentPlan.put("paymentMean", paymentMean);
		billingAccount.put("paymentPlan", paymentPlan);

		commonSub.putOnce("brandName", brandName);
		commonSub.putOnce("sourceSystem", sourceSystem);
		commonSub.putOnce("language", language);
		requestSub.put("externalID", "123456");
		requestSub.put("orderDate", getDateandTime());
		requestSub.put("relatedParties", relatedParties);
		requestSub.put("orderItems", orderItems);
		requestSub.put("billingAccount", billingAccount);
		finalJson.put("common", commonSub);
		finalJson.put("request", requestSub);
		return finalJson;
	}

	protected JSONObject serviceOrder_PayloadTW(String accID, String esn) {
		JSONObject finalJson = new JSONObject();
		JSONObject commonSub = new JSONObject();
		JSONObject requestSub = new JSONObject();
		JSONArray relatedParties = new JSONArray();
		JSONObject rpObject1 = new JSONObject();
		JSONObject rpObject2 = new JSONObject();
		JSONObject party1 = new JSONObject();
		JSONObject party2 = new JSONObject();
		JSONObject individual = new JSONObject();
		JSONArray partyExtension = new JSONArray();
		JSONObject PE_Object1 = new JSONObject();
		JSONObject PE_Object2 = new JSONObject();
		JSONArray orderItems = new JSONArray();
		JSONObject oiObject = new JSONObject();
		JSONObject product = new JSONObject();
		JSONObject productSpecification = new JSONObject();
		JSONArray supportingResources = new JSONArray();
		JSONArray relatedServices = new JSONArray();
		JSONObject rsObject = new JSONObject();
		JSONObject location = new JSONObject();
		JSONObject postalAddress = new JSONObject();

		commonSub.putOnce("brandName", brandName);
		commonSub.putOnce("sourceSystem", sourceSystem);
		commonSub.putOnce("language", language);
		commonSub.putOnce("subBrandName", brandName);

		individual.put("id", accID);
		party1.put("partyID", "SMWEB");
		party1.put("individual", individual);
		rpObject1.put("party", party1);
		rpObject1.put("roleType", "customer");
		PE_Object1.put("name", "partyTransactionID");
		PE_Object1.put("value", "web_1231234234424");
		PE_Object2.put("name", "sourceSystem");
		PE_Object2.put("value", sourceSystem);
		partyExtension.put(PE_Object1);
		partyExtension.put(PE_Object2);
		party2.put("partyID", "SMWEB");
		party2.put("languageAbility", language);
		party2.put("partyExtension", partyExtension);
		rpObject2.put("party", party2);
		rpObject2.put("roleType", "internal");
		relatedParties.put(rpObject1);
		relatedParties.put(rpObject2);

		productSpecification.put("brand", brandName);
		rsObject.put("id", "380");
		rsObject.put("category", "SERVICE_PLAN");
		rsObject.put("isRedeemNow", false);
		relatedServices.put(rsObject);
		product.put("productSerialNumber", esn);
		product.put("productCategory", "HANDSET");
		product.put("productSpecification", productSpecification);
		product.put("supportingResources", supportingResources);
		product.put("relatedServices", relatedServices);
		postalAddress.put("zipcode", zipcode);
		location.put("postalAddress", postalAddress);
		oiObject.put("action", "ACTIVATION");
		oiObject.put("product", product);
		oiObject.put("location", location);
		orderItems.put(oiObject);

		requestSub.put("externalID", "123456");
		requestSub.put("orderDate", getDateandTime());
		requestSub.put("relatedParties", relatedParties);
		requestSub.put("orderItems", orderItems);
		finalJson.put("common", commonSub);
		finalJson.put("request", requestSub);
		return finalJson;
	}

	protected JSONObject estimate_TWPayload(String accID, String esn) {
		JSONObject finalJson = new JSONObject();
		JSONObject commonSub = new JSONObject();
		JSONObject requestSub = new JSONObject();
		JSONArray relatedParties = new JSONArray();
		JSONObject rpObject1 = new JSONObject();
		JSONObject rpObject2 = new JSONObject();
		JSONObject party1 = new JSONObject();
		JSONObject individual = new JSONObject();
		JSONObject party2 = new JSONObject();
		JSONArray partyExtension = new JSONArray();
		JSONObject PE_Object1 = new JSONObject();
		JSONObject PE_Object2 = new JSONObject();
		JSONArray orderItems = new JSONArray();
		JSONObject oiObject = new JSONObject();
		JSONObject product = new JSONObject();
		JSONArray relatedServices = new JSONArray();
		JSONObject rsObject = new JSONObject();
		JSONArray characteristicSpecification = new JSONArray();
		JSONObject csObject = new JSONObject();
		JSONObject productOffering = new JSONObject();
		JSONObject productSpecification = new JSONObject();
		JSONArray location = new JSONArray();
		JSONObject locObject = new JSONObject();
		JSONObject address = new JSONObject();

		commonSub.putOnce("brandName", brandName);
		commonSub.putOnce("sourceSystem", sourceSystem);
		commonSub.putOnce("language", language);
		commonSub.putOnce("subBrandName", brandName);

		individual.put("id", accID);
		party1.put("individual", individual);
		rpObject1.put("party", party1);
		rpObject1.put("roleType", "customer");
		PE_Object1.put("name", "partyTransactionID");
		PE_Object1.put("value", "web_1231234234424");
		PE_Object2.put("name", "sourceSystem");
		PE_Object2.put("value", sourceSystem);
		partyExtension.put(PE_Object1);
		partyExtension.put(PE_Object2);
		party2.put("partyID", "TOTALWIRELESSWEB");
		party2.put("languageAbility", language);
		party2.put("partyExtension", partyExtension);
		rpObject2.put("party", party2);
		rpObject2.put("roleType", "internal");
		relatedParties.put(rpObject1);
		relatedParties.put(rpObject2);

		csObject.put("name", "relatedProgramId");
		csObject.put("value", "5802592");
		characteristicSpecification.put(csObject);
		rsObject.put("id", "379");
		rsObject.put("category", "SERVICE_PLAN");
		rsObject.put("isRecurring", true);
		rsObject.put("characteristicSpecification", characteristicSpecification);
		relatedServices.put(rsObject);
		product.put("productSerialNumber", esn);
		product.put("productCategory", "HANDSET");
		product.put("relatedServices", relatedServices);

		productSpecification.put("id", "TWN00035");
		productSpecification.put("brand", brandName);
		productOffering.put("id", "TWAPP00035");
		productOffering.put("productSpecification", productSpecification);

		oiObject.put("id", "1");
		oiObject.put("action", "ESTIMATE");
		oiObject.put("quantity", 1);
		oiObject.put("product", product);
		oiObject.put("productOffering", productOffering);
		orderItems.put(oiObject);

		address.put("zipcode", zipcode);
		locObject.put("addressType", "BILLING");
		locObject.put("address", address);
		location.put(locObject);

		requestSub.put("relatedParties", relatedParties);
		requestSub.put("orderDate", getDateandTime());
		requestSub.put("orderItems", orderItems);
		requestSub.put("location", location);

		finalJson.put("common", commonSub);
		finalJson.put("request", requestSub);
		return finalJson;
	}

	protected JSONObject productOrder_PayLoadTW(String accID, String esn,
			String sim, String groupid) {
		JSONObject finalJson = new JSONObject();
		JSONObject commonSub = new JSONObject();
		JSONObject requestSub = new JSONObject();
		JSONArray relatedParties = new JSONArray();
		JSONObject rpObject_1 = new JSONObject();
		JSONObject rpObject_2 = new JSONObject();
		JSONObject rpObject_3 = new JSONObject();
		JSONObject party1 = new JSONObject();
		JSONObject party2 = new JSONObject();
		JSONObject party3 = new JSONObject();
		JSONObject individual = new JSONObject();
		JSONArray partyExtension = new JSONArray();
		JSONObject partyExtension_E1 = new JSONObject();
		JSONObject partyExtension_E2 = new JSONObject();
		JSONArray orderItems = new JSONArray();
		JSONObject oiObject = new JSONObject();
		JSONObject productOffering = new JSONObject();
		JSONObject productSpecification = new JSONObject();
		JSONObject product = new JSONObject();
		JSONArray relatedServices = new JSONArray();
		JSONObject rsObject = new JSONObject();
		JSONObject location = new JSONObject();
		JSONObject postalAddress = new JSONObject();
		JSONObject billingAccount = new JSONObject();
		JSONObject paymentPlan = new JSONObject();
		JSONObject paymentMean = new JSONObject();
		JSONObject creditCard = new JSONObject();
		JSONObject billingAddress = new JSONObject();
		JSONArray characteristicSpecification = new JSONArray();
		JSONObject csObject = new JSONObject();
		JSONArray orderItemExtension = new JSONArray();
		JSONObject oieObject = new JSONObject();

		individual.put("id", accID);
		party1.put("individual", individual);
		party1.put("partyID", "CUST_HASH");
		partyExtension_E1.put("name", "partyTransactionID");
		partyExtension_E1.put("value", "web_1231234234424");
		partyExtension_E2.put("name", "sourceSystem");
		partyExtension_E2.put("value", sourceSystem);
		partyExtension.put(partyExtension_E1);
		partyExtension.put(partyExtension_E2);
		party2.put("partyID", "TOTALWIRELESSWEB");
		party2.put("languageAbility", language);
		party2.put("partyExtension", partyExtension);
		party3.put("partyID", "");
		rpObject_1.put("party", party1);
		rpObject_1.put("roleType", "customer");
		rpObject_2.put("party", party2);
		rpObject_2.put("roleType", "internal");
		rpObject_3.put("party", party3);
		rpObject_2.put("roleType", "referrer");
		relatedParties.put(rpObject_1);
		relatedParties.put(rpObject_2);
		relatedParties.put(rpObject_3);

		oiObject.put("id", "1");
		oiObject.put("quantity", "1");
		oiObject.put("action", "PROCESS");
		productOffering.put("id", "TWAPP00035");
		oiObject.put("productOffering", productOffering);
		csObject.put("name", "relatedProgramId");
		csObject.put("value", "5802592");
		characteristicSpecification.put(csObject);
		rsObject.put("id", "379");
		rsObject.put("category", "SERVICE_PLAN");
		rsObject.put("isRecurring", false);
		rsObject.put("characteristicSpecification", characteristicSpecification);
		relatedServices.put(rsObject);
		productSpecification.put("brand", brandName);
		product.put("productSpecification", productSpecification);
		product.put("relatedServices", relatedServices);
		product.put("productSerialNumber", esn);
		product.put("productCategory", "HANDSET");
		oiObject.put("product", product);
		oieObject.put("name", "groupIdentifier");
		oieObject.put("value", groupid);
		orderItemExtension.put(oieObject);
		oiObject.put("orderItemExtension", orderItemExtension);
		postalAddress.put("zipcode", zipcode);
		location.put("postalAddress", postalAddress);
		oiObject.put("location", location);
		orderItems.put(oiObject);

		creditCard.put("year", "2020");
		creditCard.put("month", "11");
		creditCard.put("cvv", "234");
		creditCard.put("accountNumber", "4556941005662411");
		creditCard.put("type", "VISA");
		billingAddress.put("addressLine1", "no-1234");
		billingAddress.put("addressLine2", "charleston road");
		billingAddress.put("city", "Miami");
		billingAddress.put("stateOrProvince", "FL");
		billingAddress.put("zipCode", zipcode);
		billingAddress.put("country", "USA");
		paymentMean.put("isDefault", false);
		paymentMean.put("savePaymentInfo", true);
		paymentMean.put("type", "CREDITCARD");
		paymentMean.put("accountHolderName", "Cyber Source");
		paymentMean.put("firstName", "Cyber");
		paymentMean.put("lastName", "Source");
		paymentMean.put("creditCard", creditCard);
		paymentMean.put("billingAddress", billingAddress);
		paymentPlan.put("paymentMean", paymentMean);
		billingAccount.put("paymentPlan", paymentPlan);

		commonSub.putOnce("brandName", brandName);
		commonSub.putOnce("sourceSystem", sourceSystem);
		commonSub.putOnce("language", language);
		requestSub.put("externalID", "123456");
		requestSub.put("orderDate", getDateandTime());
		requestSub.put("relatedParties", relatedParties);
		requestSub.put("orderItems", orderItems);
		requestSub.put("billingAccount", billingAccount);
		finalJson.put("common", commonSub);
		finalJson.put("request", requestSub);
		return finalJson;
	}

	protected JSONObject service_QualificationJSONTW(String esn, String sim,
			String serv_Category, String roletype) {
		JSONObject finalJson = new JSONObject();
		JSONObject commonSub = new JSONObject();
		JSONObject requestSub = new JSONObject();

		JSONArray relatedParties = new JSONArray();
		JSONArray serviceCategory = new JSONArray();
		JSONObject serviceSpecification = new JSONObject();
		JSONObject location = new JSONObject();
		JSONArray resources = new JSONArray();
		JSONObject scObject = new JSONObject();
		JSONObject rpObject = new JSONObject();
		JSONObject party = new JSONObject();
		JSONArray partyExtension = new JSONArray();
		JSONObject partyExtension_E1 = new JSONObject();
		JSONObject partyExtension_E2 = new JSONObject();
		JSONObject handsetResource = new JSONObject();
		JSONObject postalAddress = new JSONObject();

		commonSub.putOnce("brandName", brandName);
		commonSub.putOnce("sourceSystem", sourceSystem);
		commonSub.putOnce("language", language);
		commonSub.putOnce("subBrandName", brandName);

		postalAddress.put("zipcode", zipcode);
		location.put("postalAddress", postalAddress);

		scObject.put("type", "context");
		scObject.put("value", serv_Category);
		serviceCategory.put(scObject);

		serviceSpecification.put("brand", brandName);

		handsetResource.put("serialNumber", esn);
		handsetResource.put("name", "productSerialNumber");
		handsetResource.put("type", "HANDSET");
		resources.put(handsetResource);

		if (serv_Category == "ACTIVATION") {
			partyExtension_E1.put("name", "partyTransactionID");
			partyExtension_E1.put("value", "web_1231234234424");
			partyExtension_E2.put("name", "sourceSystem");
			partyExtension_E2.put("value", sourceSystem);
			partyExtension.put(partyExtension_E1);
			partyExtension.put(partyExtension_E2);
			party.put("partyID", "TOTALWIRELESSWEB");
			party.put("languageAbility", language);
			party.put("partyExtension", partyExtension);
			rpObject.put("roleType", roletype);
			rpObject.put("party", party);
			relatedParties.put(rpObject);
			requestSub.put("relatedParties", relatedParties);
		}

		requestSub.put("location", location);
		requestSub.put("serviceCategory", serviceCategory);
		requestSub.put("serviceSpecification", serviceSpecification);
		requestSub.put("resources", resources);

		finalJson.put("common", commonSub);
		finalJson.put("request", requestSub);
		return finalJson;
	}

	protected JSONObject service_QualificationJSON_NTSM(String esn, String sim,
			String serv_Category, String pID) {
		JSONObject finalJson = new JSONObject();
		JSONObject commonSub = new JSONObject();
		JSONObject requestSub = new JSONObject();

		JSONArray relatedParties = new JSONArray();
		JSONArray serviceCategory = new JSONArray();
		JSONObject serviceSpecification = new JSONObject();
		JSONObject location = new JSONObject();
		JSONArray resources = new JSONArray();
		JSONObject scObject = new JSONObject();
		JSONObject rpObject = new JSONObject();
		JSONObject party = new JSONObject();
		JSONArray partyExtension = new JSONArray();
		JSONObject partyExtension_E1 = new JSONObject();
		JSONObject partyExtension_E2 = new JSONObject();
		JSONObject handsetResource = new JSONObject();
		JSONObject simResource = new JSONObject();
		JSONObject postalAddress = new JSONObject();

		commonSub.putOnce("brandName", brandName);
		commonSub.putOnce("sourceSystem", sourceSystem);
		commonSub.putOnce("language", language);
		commonSub.putOnce("subBrandName", brandName);

		postalAddress.put("zipcode", zipcode);
		location.put("postalAddress", postalAddress);

		scObject.put("type", "context");
		scObject.put("value", serv_Category);
		serviceCategory.put(scObject);

		serviceSpecification.put("brand", brandName);

		handsetResource.put("serialNumber", esn);
		handsetResource.put("name", "productSerialNumber");
		handsetResource.put("type", "HANDSET");
		simResource.put("serialNumber", sim);
		simResource.put("name", "serialNumber");
		simResource.put("type", "SIM_CARD");
		resources.put(handsetResource);
		resources.put(simResource);

		if (serv_Category == "ACTIVATION") {
			partyExtension_E1.put("name", "partyTransactionID");
			partyExtension_E1.put("value", "web_1231234234424");
			partyExtension_E2.put("name", "sourceSystem");
			partyExtension_E2.put("value", sourceSystem);
			partyExtension.put(partyExtension_E1);
			partyExtension.put(partyExtension_E2);
			party.put("partyID", pID);
			party.put("languageAbility", language);
			party.put("partyExtension", partyExtension);
			rpObject.put("roleType", "internal");
			rpObject.put("party", party);
			relatedParties.put(rpObject);
			requestSub.put("relatedParties", relatedParties);
		}

		requestSub.put("location", location);
		requestSub.put("serviceCategory", serviceCategory);
		requestSub.put("serviceSpecification", serviceSpecification);
		requestSub.put("resources", resources);

		finalJson.put("common", commonSub);
		finalJson.put("request", requestSub);
		return finalJson;
	}

	protected JSONObject estimate_SMPayload(String accID, String esn) {
		JSONObject finalJson = new JSONObject();
		JSONObject commonSub = new JSONObject();
		JSONObject requestSub = new JSONObject();
		JSONArray relatedParties = new JSONArray();
		JSONObject rpObject1 = new JSONObject();
		JSONObject rpObject2 = new JSONObject();
		JSONObject party1 = new JSONObject();
		JSONObject individual = new JSONObject();
		JSONObject party2 = new JSONObject();
		JSONArray partyExtension = new JSONArray();
		JSONObject PE_Object1 = new JSONObject();
		JSONObject PE_Object2 = new JSONObject();
		JSONArray orderItems = new JSONArray();
		JSONObject oiObject = new JSONObject();
		JSONObject product = new JSONObject();
		JSONArray relatedServices = new JSONArray();
		JSONObject rsObject = new JSONObject();
		JSONArray characteristicSpecification = new JSONArray();
		JSONObject csObject = new JSONObject();
		JSONObject productOffering = new JSONObject();
		JSONObject productSpecification = new JSONObject();
		JSONArray location = new JSONArray();
		JSONObject locObject = new JSONObject();
		JSONObject address = new JSONObject();
		JSONObject billingAccount = new JSONObject();
		JSONObject paymentPlan = new JSONObject();

		commonSub.putOnce("brandName", brandName);
		commonSub.putOnce("sourceSystem", sourceSystem);
		commonSub.putOnce("language", language);
		commonSub.putOnce("subBrandName", brandName);

		individual.put("id", accID);
		party1.put("individual", individual);
		rpObject1.put("party", party1);
		rpObject1.put("roleType", "customer");
		PE_Object1.put("name", "partyTransactionID");
		PE_Object1.put("value", "web_1231234234424");
		PE_Object2.put("name", "sourceSystem");
		PE_Object2.put("value", sourceSystem);
		partyExtension.put(PE_Object1);
		partyExtension.put(PE_Object2);
		party2.put("partyID", "SMWEB");
		party2.put("languageAbility", language);
		party2.put("partyExtension", partyExtension);
		rpObject2.put("party", party2);
		rpObject2.put("roleType", "internal");
		relatedParties.put(rpObject1);
		relatedParties.put(rpObject2);

		csObject.put("name", "relatedProgramId");
		csObject.put("value", "5801500");
		characteristicSpecification.put(csObject);
		rsObject.put("id", "235");
		rsObject.put("category", "SERVICE_PLAN");
		rsObject.put("isRecurring", true);
		rsObject.put("characteristicSpecification", characteristicSpecification);
		relatedServices.put(rsObject);
		product.put("productSerialNumber", esn);
		product.put("productCategory", "HANDSET");
		product.put("relatedServices", relatedServices);

		productSpecification.put("id", "SM025TT");
		productSpecification.put("brand", brandName);
		productOffering.put("id", "SMNAPP0025TT");
		productOffering.put("productSpecification", productSpecification);

		oiObject.put("id", "1");
		oiObject.put("action", "ESTIMATE");
		oiObject.put("quantity", 1);
		oiObject.put("product", product);
		oiObject.put("productOffering", productOffering);
		orderItems.put(oiObject);

		address.put("zipcode", zipcode);
		locObject.put("addressType", "BILLING");
		locObject.put("address", address);
		location.put(locObject);

		paymentPlan.put("isRecurring", true);
		billingAccount.put("paymentPlan", paymentPlan);

		requestSub.put("billingAccount", billingAccount);
		requestSub.put("relatedParties", relatedParties);
		requestSub.put("orderDate", getDateandTime());
		requestSub.put("orderItems", orderItems);
		requestSub.put("location", location);
		requestSub.put("type", "ACTIVATE_BAN");

		finalJson.put("common", commonSub);
		finalJson.put("request", requestSub);
		return finalJson;
	}

	protected JSONObject productOrder_PayloadSM(String accID, String esn,
			String sim) {
		JSONObject finalJson = new JSONObject();
		JSONObject commonSub = new JSONObject();
		JSONObject requestSub = new JSONObject();
		JSONArray relatedParties = new JSONArray();
		JSONObject rpObject_1 = new JSONObject();
		JSONObject rpObject_2 = new JSONObject();
		JSONObject rpObject_3 = new JSONObject();
		JSONObject party1 = new JSONObject();
		JSONObject party2 = new JSONObject();
		JSONObject party3 = new JSONObject();
		JSONObject individual = new JSONObject();
		JSONArray partyExtension = new JSONArray();
		JSONObject partyExtension_E1 = new JSONObject();
		JSONObject partyExtension_E2 = new JSONObject();
		JSONArray orderItems = new JSONArray();
		JSONObject oiObject = new JSONObject();
		JSONObject productOffering = new JSONObject();
		JSONObject productSpecification1 = new JSONObject();
		JSONObject productSpecification2 = new JSONObject();
		JSONArray supportingResources = new JSONArray();
		JSONObject srObject = new JSONObject();
		JSONObject product = new JSONObject();
		JSONArray relatedServices = new JSONArray();
		JSONObject rsObject = new JSONObject();
		JSONObject location = new JSONObject();
		JSONObject postalAddress = new JSONObject();
		JSONObject billingAccount = new JSONObject();
		JSONObject paymentPlan = new JSONObject();
		JSONObject paymentMean = new JSONObject();
		JSONObject creditCard = new JSONObject();
		JSONObject billingAddress = new JSONObject();
		JSONArray characteristicSpecification = new JSONArray();
		JSONObject csObject = new JSONObject();

		individual.put("id", accID);
		party1.put("individual", individual);
		party1.put("partyID", "CUST_HASH");
		partyExtension_E1.put("name", "partyTransactionID");
		partyExtension_E1.put("value", "web_1231234234424");
		partyExtension_E2.put("name", "sourceSystem");
		partyExtension_E2.put("value", sourceSystem);
		partyExtension.put(partyExtension_E1);
		partyExtension.put(partyExtension_E2);
		party2.put("partyID", "SMWEB");
		party2.put("languageAbility", language);
		party2.put("partyExtension", partyExtension);
		party3.put("partyID", "");
		rpObject_1.put("party", party1);
		rpObject_1.put("roleType", "customer");
		rpObject_2.put("party", party2);
		rpObject_2.put("roleType", "internal");
		rpObject_3.put("party", party3);
		rpObject_2.put("roleType", "referrer");
		relatedParties.put(rpObject_1);
		relatedParties.put(rpObject_2);
		relatedParties.put(rpObject_3);

		oiObject.put("id", "1");
		oiObject.put("quantity", "1");
		oiObject.put("action", "ACTIVATION");
		productOffering.put("id", "SMNAPP0025TT");
		productSpecification1.put("brand", brandName);
		productSpecification1.put("id", "SM025TT");
		productOffering.put("productSpecification", productSpecification1);
		oiObject.put("productOffering", productOffering);
		csObject.put("name", "relatedProgramId");
		csObject.put("value", "5801500");
		characteristicSpecification.put(csObject);
		rsObject.put("id", "235");
		rsObject.put("category", "SERVICE_PLAN");
		rsObject.put("isRecurring", true);
		rsObject.put("characteristicSpecification", characteristicSpecification);
		relatedServices.put(rsObject);
		productSpecification2.put("brand", brandName);
		product.put("productSpecification", productSpecification2);
		srObject.put("resourceType", "SIM_CARD");
		srObject.put("serialNumber", sim);
		supportingResources.put(srObject);
		product.put("relatedServices", relatedServices);
		product.put("supportingResources", supportingResources);
		product.put("productSerialNumber", esn);
		product.put("productCategory", "HANDSET");
		oiObject.put("product", product);
		postalAddress.put("zipcode", zipcode);
		location.put("postalAddress", postalAddress);
		oiObject.put("location", location);
		orderItems.put(oiObject);

		creditCard.put("year", "2020");
		creditCard.put("month", "11");
		creditCard.put("cvv", "234");
		creditCard.put("accountNumber", "4556941005662411");
		creditCard.put("type", "VISA");
		billingAddress.put("addressLine1", "no-1234");
		billingAddress.put("addressLine2", "charleston road");
		billingAddress.put("city", "Miami");
		billingAddress.put("stateOrProvince", "FL");
		billingAddress.put("zipCode", zipcode);
		billingAddress.put("country", "USA");
		paymentMean.put("isDefault", false);
		paymentMean.put("savePaymentInfo", true);
		paymentMean.put("type", "CREDITCARD");
		paymentMean.put("accountHolderName", "Cyber Source");
		paymentMean.put("firstName", "Cyber");
		paymentMean.put("lastName", "Source");
		paymentMean.put("creditCard", creditCard);
		paymentMean.put("billingAddress", billingAddress);
		paymentPlan.put("paymentMean", paymentMean);
		paymentPlan.put("isRecurring", true);
		billingAccount.put("paymentPlan", paymentPlan);

		commonSub.putOnce("brandName", brandName);
		commonSub.putOnce("sourceSystem", sourceSystem);
		commonSub.putOnce("language", language);
		requestSub.put("externalID", "123456");
		requestSub.put("orderDate", getDateandTime());
		requestSub.put("relatedParties", relatedParties);
		requestSub.put("orderItems", orderItems);
		requestSub.put("billingAccount", billingAccount);
		requestSub.put("type", "ACTIVATE_BAN");
		finalJson.put("common", commonSub);
		finalJson.put("request", requestSub);
		return finalJson;
	}

	protected JSONObject service_QualificationJSON_WFM(String esn, String sim,
			String serv_Category) {
		JSONObject finalJson = new JSONObject();
		JSONObject commonSub = new JSONObject();
		JSONObject requestSub = new JSONObject();

		JSONArray relatedParties = new JSONArray();
		JSONArray serviceCategory = new JSONArray();
		JSONObject serviceSpecification = new JSONObject();
		JSONObject location = new JSONObject();
		JSONObject scObject = new JSONObject();
		JSONObject rpObject = new JSONObject();
		JSONObject party = new JSONObject();
		JSONArray partyExtension = new JSONArray();
		JSONObject partyExtension_E1 = new JSONObject();
		JSONObject partyExtension_E2 = new JSONObject();
		JSONObject handsetResource = new JSONObject();
		JSONObject simResource = new JSONObject();
		JSONObject postalAddress = new JSONObject();

		commonSub.putOnce("brandName", brandName);
		commonSub.putOnce("sourceSystem", sourceSystem);
		commonSub.putOnce("language", language);

		postalAddress.put("zipcode", zipcode);
		location.put("postalAddress", postalAddress);

		scObject.put("type", "context");
		scObject.put("value", serv_Category);
		serviceCategory.put(scObject);
		serviceSpecification.put("brand", brandName);
		partyExtension_E1.put("name", "partyTransactionID");
		partyExtension_E1.put("value", "web_1231234234424");
		partyExtension_E2.put("name", "sourceSystem");
		partyExtension_E2.put("value", sourceSystem);
		partyExtension.put(partyExtension_E1);
		partyExtension.put(partyExtension_E2);
		party.put("partyID", "WFMWEB");
		party.put("languageAbility", language);
		party.put("partyExtension", partyExtension);
		rpObject.put("roleType", "application");
		rpObject.put("party", party);
		relatedParties.put(rpObject);

		if (serv_Category == "ACH_ELIGIBILITY") {
			JSONObject resources = new JSONObject();
			requestSub.put("resources", resources);
		} else {
			JSONArray resources = new JSONArray();
			handsetResource.put("serialNumber", esn);
			handsetResource.put("name", "productSerialNumber");
			handsetResource.put("type", "HANDSET");
			simResource.put("serialNumber", sim);
			simResource.put("name", "serialNumber");
			simResource.put("type", "SIM_CARD");
			resources.put(handsetResource);
			resources.put(simResource);
			requestSub.put("resources", resources);
		}

		requestSub.put("location", location);
		requestSub.put("serviceCategory", serviceCategory);
		requestSub.put("serviceSpecification", serviceSpecification);
		requestSub.put("relatedParties", relatedParties);

		finalJson.put("common", commonSub);
		finalJson.put("request", requestSub);
		return finalJson;
	}

	protected JSONObject estimate_WFMPayload(String accID, String esn) {
		JSONObject finalJson = new JSONObject();
		JSONObject commonSub = new JSONObject();
		JSONObject requestSub = new JSONObject();
		JSONArray relatedParties = new JSONArray();
		JSONObject rpObject1 = new JSONObject();
		JSONObject rpObject2 = new JSONObject();
		JSONObject party1 = new JSONObject();
		JSONObject individual = new JSONObject();
		JSONObject party2 = new JSONObject();
		JSONArray partyExtension = new JSONArray();
		JSONObject PE_Object1 = new JSONObject();
		JSONObject PE_Object2 = new JSONObject();
		JSONArray orderItems = new JSONArray();
		JSONObject oiObject = new JSONObject();
		JSONObject product = new JSONObject();
		JSONArray relatedServices = new JSONArray();
		JSONObject rsObject = new JSONObject();
		JSONArray characteristicSpecification = new JSONArray();
		JSONObject csObject = new JSONObject();
		JSONObject productOffering = new JSONObject();
		JSONObject productSpecification = new JSONObject();
		JSONArray location = new JSONArray();
		JSONObject locObject = new JSONObject();
		JSONObject address = new JSONObject();
		JSONObject billingAccount = new JSONObject();

		commonSub.putOnce("brandName", brandName);
		commonSub.putOnce("sourceSystem", sourceSystem);
		commonSub.putOnce("language", language);

		individual.put("id", accID);
		party1.put("individual", individual);
		rpObject1.put("party", party1);
		rpObject1.put("roleType", "customer");
		PE_Object1.put("name", "partyTransactionID");
		PE_Object1.put("value", "web_1231234234424");
		PE_Object2.put("name", "sourceSystem");
		PE_Object2.put("value", sourceSystem);
		partyExtension.put(PE_Object1);
		partyExtension.put(PE_Object2);
		party2.put("partyID", "WFMWEB");
		party2.put("languageAbility", language);
		party2.put("partyExtension", partyExtension);
		rpObject2.put("party", party2);
		rpObject2.put("roleType", "internal");
		relatedParties.put(rpObject1);
		relatedParties.put(rpObject2);

		csObject.put("name", "relatedProgramId");
		csObject.put("value", "5801500");
		characteristicSpecification.put(csObject);
		rsObject.put("id", "481");
		rsObject.put("category", "SERVICE_PLAN");
		rsObject.put("isRecurring", true);
		rsObject.put("characteristicSpecification", characteristicSpecification);
		relatedServices.put(rsObject);
		product.put("productSerialNumber", esn);
		product.put("productCategory", "HANDSET");
		product.put("relatedServices", relatedServices);

		productSpecification.put("id", "WFMU0024");
		productSpecification.put("brand", brandName);
		productOffering.put("id", "WFMAPPU0024");
		productOffering.put("productSpecification", productSpecification);

		oiObject.put("id", "1");
		oiObject.put("action", "ESTIMATE");
		oiObject.put("quantity", 1);
		oiObject.put("product", product);
		oiObject.put("productOffering", productOffering);
		orderItems.put(oiObject);

		address.put("zipcode", zipcode);
		locObject.put("addressType", "BILLING");
		locObject.put("address", address);
		location.put(locObject);

		requestSub.put("billingAccount", billingAccount);
		requestSub.put("relatedParties", relatedParties);
		requestSub.put("orderDate", getDateandTime());
		requestSub.put("orderItems", orderItems);
		requestSub.put("location", location);
		requestSub.put("type", "ACTIVATE_BAN");

		finalJson.put("common", commonSub);
		finalJson.put("request", requestSub);
		return finalJson;
	}

	protected JSONObject productOrder_PayloadWFM(String accID, String esn,
			String sim) {
		JSONObject finalJson = new JSONObject();
		JSONObject commonSub = new JSONObject();
		JSONObject requestSub = new JSONObject();
		JSONArray relatedParties = new JSONArray();
		JSONObject rpObject1 = new JSONObject();
		JSONObject rpObject2 = new JSONObject();
		JSONObject party1 = new JSONObject();
		JSONObject individual = new JSONObject();
		JSONObject party2 = new JSONObject();
		JSONArray partyExtension = new JSONArray();
		JSONObject PE_Object1 = new JSONObject();
		JSONObject PE_Object2 = new JSONObject();
		JSONArray orderItems = new JSONArray();
		JSONObject oiObject = new JSONObject();
		JSONObject productOffering = new JSONObject();
		JSONObject productSpecification1 = new JSONObject();
		JSONObject productSpecification2 = new JSONObject();
		JSONArray supportingResources = new JSONArray();
		JSONObject srObject = new JSONObject();
		JSONObject product = new JSONObject();
		JSONArray relatedServices = new JSONArray();
		JSONObject rsObject = new JSONObject();
		JSONObject location = new JSONObject();
		JSONObject postalAddress = new JSONObject();
		JSONObject billingAccount = new JSONObject();
		JSONObject paymentPlan = new JSONObject();
		JSONObject paymentMean = new JSONObject();
		JSONObject creditCard = new JSONObject();
		JSONObject billingAddress = new JSONObject();
		JSONObject customerAccount = new JSONObject();

		individual.put("id", accID);
		party1.put("individual", individual);
		rpObject1.put("party", party1);
		rpObject1.put("roleType", "customer");
		PE_Object1.put("name", "partyTransactionID");
		PE_Object1.put("value", "web_1231234234424");
		PE_Object2.put("name", "sourceSystem");
		PE_Object2.put("value", sourceSystem);
		partyExtension.put(PE_Object1);
		partyExtension.put(PE_Object2);
		party2.put("partyID", "WFMWEB");
		party2.put("languageAbility", language);
		party2.put("partyExtension", partyExtension);
		rpObject2.put("party", party2);
		rpObject2.put("roleType", "internal");
		relatedParties.put(rpObject1);
		relatedParties.put(rpObject2);

		oiObject.put("id", "1");
		oiObject.put("quantity", "1");
		oiObject.put("action", "ACTIVATION");
		productOffering.put("id", "WFMAPPU0024");
		productSpecification1.put("brand", brandName);
		productSpecification1.put("id", "WFMU0024");
		productOffering.put("productSpecification", productSpecification1);
		oiObject.put("productOffering", productOffering);
		rsObject.put("id", "481");
		rsObject.put("category", "SERVICE_PLAN");
		rsObject.put("isRecurring", false);
		relatedServices.put(rsObject);
		productSpecification2.put("brand", brandName);
		product.put("productSpecification", productSpecification2);
		srObject.put("resourceType", "SIM_CARD");
		srObject.put("serialNumber", sim);
		supportingResources.put(srObject);
		product.put("relatedServices", relatedServices);
		product.put("supportingResources", supportingResources);
		product.put("productSerialNumber", esn);
		product.put("productCategory", "HANDSET");
		oiObject.put("product", product);
		postalAddress.put("zipcode", zipcode);
		location.put("postalAddress", postalAddress);
		oiObject.put("location", location);
		customerAccount.put("pin", "1234");
		oiObject.put("customerAccount", customerAccount);
		orderItems.put(oiObject);

		creditCard.put("year", "2020");
		creditCard.put("month", "11");
		creditCard.put("cvv", "234");
		creditCard.put("accountNumber", "4556941005662411");
		creditCard.put("type", "VISA");
		billingAddress.put("addressLine1", "no-1234");
		billingAddress.put("addressLine2", "charleston road");
		billingAddress.put("city", "Miami");
		billingAddress.put("stateOrProvince", "FL");
		billingAddress.put("zipCode", zipcode);
		billingAddress.put("country", "USA");
		paymentMean.put("isDefault", false);
		paymentMean.put("savePaymentInfo", true);
		paymentMean.put("type", "CREDITCARD");
		paymentMean.put("accountHolderName", "Cyber Source");
		paymentMean.put("firstName", "Cyber");
		paymentMean.put("lastName", "Source");
		paymentMean.put("creditCard", creditCard);
		paymentMean.put("billingAddress", billingAddress);
		paymentPlan.put("paymentMean", paymentMean);
		paymentPlan.put("isRecurring", false);
		billingAccount.put("paymentPlan", paymentPlan);

		commonSub.putOnce("brandName", brandName);
		commonSub.putOnce("sourceSystem", sourceSystem);
		commonSub.putOnce("language", language);
		requestSub.put("externalID", "123456");
		requestSub.put("orderDate", getDateandTime());
		requestSub.put("relatedParties", relatedParties);
		requestSub.put("orderItems", orderItems);
		requestSub.put("billingAccount", billingAccount);
		requestSub.put("type", "ACTIVATE_BAN");
		finalJson.put("common", commonSub);
		finalJson.put("request", requestSub);
		return finalJson;
	}

}